package org.fenrir.yggdrasil.core.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.util.List;
import javax.inject.Inject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.exception.PreferenceException;
import org.fenrir.yggdrasil.core.module.CoreStartupModule;
import org.fenrir.yggdrasil.core.service.IWorkspaceAdministrationService;

@Guice(modules = CoreStartupModule.class)
public class WorkspaceAdministrationTestCase 
{
	private static final String WORKSPACE_FILE = "workspaces.xml";
	
	private File tempWorkspace;
	
	@Inject
	private IWorkspaceAdministrationService workspaceAdministrationService;
	
	public void setWorkspaceAdministrationService(IWorkspaceAdministrationService workspaceAdministrationService)
	{
		this.workspaceAdministrationService = workspaceAdministrationService;
	}
	
	@BeforeTest
	public void setUpTest() throws IOException
	{
		File workspaceFile = new File(WORKSPACE_FILE);
		if(workspaceFile.exists()){
			workspaceFile.delete();
		}
		
		Path tempWorkspacePath = Files.createTempDirectory("workspace", new FileAttribute<?>[]{});
		tempWorkspace = new File(tempWorkspacePath.toString());
		tempWorkspace.deleteOnExit();
	}
	
	@AfterTest
	public void tearDownTest()
	{
		File workspaceFile = new File(WORKSPACE_FILE);
		if(workspaceFile.exists()){
			workspaceFile.delete();
		}
	}
	
	@Test
	public void loadEmptyWorkspaceTest() throws ApplicationException, PreferenceException
	{
		workspaceAdministrationService.loadDefaultWorkspace();
	}
	
	@Test(dependsOnMethods="loadEmptyWorkspaceTest")
	public void addWorkspaceTest() throws ApplicationException, PreferenceException
	{
		workspaceAdministrationService.addWorkspace(tempWorkspace.getAbsolutePath(), true);
		
		List<String> workspaces = workspaceAdministrationService.getWorkspacesList();
		Assert.assertTrue(!workspaces.isEmpty());
		Assert.assertTrue(workspaces.contains(tempWorkspace.getAbsolutePath()));
	}
	
	@Test(dependsOnMethods="addWorkspaceTest")
	public void loadWorkspaceTest() throws ApplicationException, PreferenceException
	{
		workspaceAdministrationService.loadWorkspace(tempWorkspace.getAbsolutePath());
		Assert.assertEquals(workspaceAdministrationService.getCurrentWorkspaceFolder(), tempWorkspace.getAbsolutePath());
		File corePreferencesFile = new File(tempWorkspace.getAbsoluteFile() + File.separator + "preferences" + File.separator + "org.fenrir.yggdrasil.core.xml");
		Assert.assertTrue(corePreferencesFile.exists());
	}
}
