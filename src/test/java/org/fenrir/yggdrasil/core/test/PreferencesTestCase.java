package org.fenrir.yggdrasil.core.test;

import java.net.URL;
import javax.inject.Inject;
import org.testng.Assert;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;
import org.fenrir.yggdrasil.core.CoreConstants;
import org.fenrir.yggdrasil.core.exception.PreferenceException;
import org.fenrir.yggdrasil.core.module.CoreStartupModule;
import org.fenrir.yggdrasil.core.service.IPreferenceService;

@Guice(modules = CoreStartupModule.class)
public class PreferencesTestCase 
{
	@Inject
	private IPreferenceService preferenceService;
	
	public void setPreferenceService(IPreferenceService preferenceService)
	{
		this.preferenceService = preferenceService;
	}
	
	@Test
	public void coreModulePreferencesTest() throws PreferenceException
	{
		URL configurationUrl = getClass().getClassLoader().getResource("org/fenrir/yggdrasil/core/test/conf/org.fenrir.yggdrasil.core.test.xml");
        preferenceService.loadPreferencesFile(IPreferenceService.FRAMEWORK_PREFERENCES_ID, configurationUrl);
        
        Assert.assertEquals(preferenceService.getProperty(CoreConstants.PREFERENCES_PROXY_HOST), "http://localhost");
        Assert.assertEquals(preferenceService.getProperty(CoreConstants.PREFERENCES_PROXY_PORT), "123");
	}
	
	@Test
	public void multiplePreferencesTest() throws PreferenceException
	{
		URL configurationUrl = getClass().getClassLoader().getResource("org/fenrir/yggdrasil/core/test/conf/testPreferences_1.xml");
        preferenceService.loadPreferencesFile("test.1", configurationUrl);
        
        configurationUrl = getClass().getClassLoader().getResource("org/fenrir/yggdrasil/core/test/conf/testPreferences_2.xml");
        preferenceService.loadPreferencesFile("test.2", configurationUrl);
        
        Assert.assertEquals(preferenceService.getProperty("//preferences/tasks/task[contains(@name, 'org.fenrir.yggdrasil.core.task.updateApplicationTask')]/class"), 
        		"org.fenrir.yggdrasil.core.task.UpdateApplicationTask");
        Assert.assertEquals(preferenceService.getProperty("//preferences[@id='test.1']/ui/lookandfeel/skin"), 
        		"org.pushingpixels.substance.api.skin.SubstanceDustLookAndFeel");
	}
}
