package org.fenrir.yggdrasil.core.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131203
 */
public class DefaultExceptionHandler implements Thread.UncaughtExceptionHandler
{
    private final Logger log = LoggerFactory.getLogger(DefaultExceptionHandler.class);
    
    private final List<IErrorNotificationListener> notificationListeners = Collections.synchronizedList(new ArrayList<IErrorNotificationListener>());
    
    public void addErrorNotificationListener(IErrorNotificationListener listener)
    {
        notificationListeners.add(listener);
    }
    
    public void removeErrorNotificationListener(IErrorNotificationListener listener)
    {
        notificationListeners.remove(listener);
    }
    
    public void clearErrorNotificationListeners()
    {
        notificationListeners.clear();
    }
    
    @Override
    public void uncaughtException(Thread t, Throwable e) 
    {
        log.error("Error inesperat al thread {}: {}", new Object[]{t, e.getMessage(), e});
        synchronized(notificationListeners){
            for(IErrorNotificationListener listener:notificationListeners){
                listener.errorNotified(e.getMessage(), e);
            }
        }
    }
    
    public static interface IErrorNotificationListener
    {
        public void errorNotified(String message, Throwable error);
    }
}
