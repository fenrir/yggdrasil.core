package org.fenrir.yggdrasil.core.util;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140412
 */
public interface ICallback<T, U> 
{
	public U execute(T cArg);
}
