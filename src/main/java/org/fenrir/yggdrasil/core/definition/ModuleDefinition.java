package org.fenrir.yggdrasil.core.definition;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140314
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ModuleDefinition 
{
	@XmlAttribute
    private String id;
	@XmlElement(name="class")
    private String className;
	@XmlElement
    private String executionContext;

    public String getId() 
    {
        return id;
    }

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getClassName() 
    {
        return className;
    }

    public void setClassName(String className) 
    {
        this.className = className;
    }

    public String getExecutionContext() 
    {
        return executionContext;
    }

    public void setExecutionContext(String executionContext) 
    {
        this.executionContext = executionContext;
    }        
}
