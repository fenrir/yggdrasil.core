package org.fenrir.yggdrasil.core.definition;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140316
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ArtifactsDefinition
{
    @XmlAttribute
    private String file;

    public String getFile() 
    {
        return file;
    }

    public void setFile(String file) 
    {
        this.file = file;
    }
}
