package org.fenrir.yggdrasil.core.definition;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20141011
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PreferencesDefinition 
{
	@XmlAttribute
	private String id;
	
	@XmlElement(name="default")
    private PreferencesFileDefinition defaultFile;
    
	@XmlElement(name="update")
    private PreferencesFileDefinition updateFile;    

	public String getId()
	{
		return id;
	}
	
	public void setId(String id)
	{
		this.id = id;
	}
	
    public PreferencesFileDefinition getDefaultFile() 
    {
        return defaultFile;
    }

    public void setDefaultFile(PreferencesFileDefinition defaultFile) 
    {
        this.defaultFile = defaultFile;
    }

    public PreferencesFileDefinition getUpdateFile() 
    {
        return updateFile;
    }

    public void setUpdateFile(PreferencesFileDefinition updateFile) 
    {
        this.updateFile = updateFile;
    }
}
