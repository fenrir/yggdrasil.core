package org.fenrir.yggdrasil.core.definition;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20141011
 */
@XmlRootElement(name="application")
@XmlAccessorType(XmlAccessType.FIELD)
public class ApplicationDefinition extends PluginDefinition 
{
    @XmlElement
    private ProductDefinition product;
    
    @XmlElement
    private ArtifactsDefinition artifacts;

    public ProductDefinition getProduct()
    {
        return product;
    }

    public void setProduct(ProductDefinition product) 
    {
        this.product = product;
    }

    public ArtifactsDefinition getArtifacts()
    {
        return artifacts;
    }
    
    public void setArtifacts(ArtifactsDefinition artifacts)
    {
        this.artifacts = artifacts;
    }
}
