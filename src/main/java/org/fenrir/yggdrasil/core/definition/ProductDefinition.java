package org.fenrir.yggdrasil.core.definition;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140315
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductDefinition 
{
	@XmlElement
    private String name;
	@XmlElement
    private String fullVersionName;
	@XmlElement
    private String version;
	@XmlElement
    private String vendor;
	@XmlElement
	private String icon;

    public String getName() 
    {
        return name;
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public String getFullVersionName() 
    {
        return fullVersionName;
    }

    public void setFullVersionName(String fullVersionName) 
    {
        this.fullVersionName = fullVersionName;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version) 
    {
        this.version = version;
    }

    public String getVendor() 
    {
        return vendor;
    }

    public void setVendor(String vendor) 
    {
        this.vendor = vendor;
    }
    
    public String getIcon()
    {
    	return icon;
    }
    
    public void setIcon(String icon)
    {
    	this.icon = icon;
    }
}
