@javax.xml.bind.annotation.XmlSchema(
	xmlns = { 
		@javax.xml.bind.annotation.XmlNs(prefix="fw",
            namespaceURI="http://www.fenrir-soft.com/yggdrasil/schema")	 
	},								
	namespace = "http://www.fenrir-soft.com/yggdrasil/schema", 
	elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED
)
package org.fenrir.yggdrasil.core.definition;
