package org.fenrir.yggdrasil.core.definition;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140314
 */
public interface IPluginDefinitionBuilder 
{
	public PluginDefinition buildDefinition(String filePath);
}
