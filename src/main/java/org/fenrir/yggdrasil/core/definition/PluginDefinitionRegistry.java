package org.fenrir.yggdrasil.core.definition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * TODO v1.0 Documentació
 * TODO Permetre contextes disjunts
 * @author Antonio Archilla Nava
 * @version v0.1.20141011
 */
public class PluginDefinitionRegistry 
{
	private ProductDefinition productDefinition;
	private List<PreferencesDefinition> preferencesDefinitions;
	private List<ModuleDefinition> moduleDefinitions;
	private ArtifactsDefinition artifactsDefinition;
	
	public ProductDefinition getProductDefinition() 
	{
		return productDefinition;
	}
	
	public void setProductDefinition(ProductDefinition productDefinition) 
	{
		this.productDefinition = productDefinition;
	}
	
	public List<PreferencesDefinition> getPreferencesDefinitions() 
	{
		if(preferencesDefinitions==null){
			return Collections.emptyList();
		}
		return preferencesDefinitions;
	}
	
	public void setPreferencesDefinitions(List<PreferencesDefinition> preferencesDefinitions) 
	{
		this.preferencesDefinitions = preferencesDefinitions;
	}
	
	public void addPreferencesDefinitions(Collection<PreferencesDefinition> preferencesDefinitions)
	{
		if(this.preferencesDefinitions==null){
			this.preferencesDefinitions = new ArrayList<PreferencesDefinition>();
		}
		this.preferencesDefinitions.addAll(preferencesDefinitions);
	}
	
	public void addPreferencesDefinition(PreferencesDefinition preferencesDefinition)
	{
		if(preferencesDefinitions==null){
			preferencesDefinitions = new ArrayList<PreferencesDefinition>();
		}
		preferencesDefinitions.add(preferencesDefinition);
	}
	
	public List<ModuleDefinition> getModuleDefinitions() 
	{
		if(moduleDefinitions==null){
			return Collections.emptyList();
		}
		return moduleDefinitions;
	}
	
	public void setModuleDefinitions(List<ModuleDefinition> moduleDefinitions) 
	{
		this.moduleDefinitions = moduleDefinitions;
	}

	public void addModuleDefinitions(Collection<ModuleDefinition> moduleDefinitions)
	{
		if(this.moduleDefinitions==null){
			this.moduleDefinitions = new ArrayList<ModuleDefinition>();
		}
		this.moduleDefinitions.addAll(moduleDefinitions);
	}
	
	public void addModuleDefinition(ModuleDefinition moduleDefinition)
	{
		if(moduleDefinitions==null){
			moduleDefinitions = new ArrayList<ModuleDefinition>();
		}
		moduleDefinitions.add(moduleDefinition);
	}
	
	public ArtifactsDefinition getArtifactsDefinition() 
	{
		return artifactsDefinition;
	}
	
	public void setArtifactsDefinition(ArtifactsDefinition artifactsDefinition) 
	{
		this.artifactsDefinition = artifactsDefinition;
	}
}
