package org.fenrir.yggdrasil.core.definition;

import java.util.Collections;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20141011
 */
@XmlRootElement(name="plugin")
@XmlAccessorType(XmlAccessType.FIELD)
public class PluginDefinition 
{
	@XmlElement(name="preferences")
	private PreferencesDefinition preferences;
	
	@XmlElement(name="module")
    @XmlElementWrapper(name="modules")
	private List<ModuleDefinition> modules;
	
    public PreferencesDefinition getPreferences()
	{
		return preferences;
	}
	
	public void setPreferences(PreferencesDefinition preferences)
	{
		this.preferences = preferences;
	}
	
	public List<ModuleDefinition> getModules()
	{
		if(modules==null){
			return Collections.emptyList();
		}
		return modules;
	}
	
	public void setModules(List<ModuleDefinition> modules)
	{
		this.modules= modules;
	}
}
