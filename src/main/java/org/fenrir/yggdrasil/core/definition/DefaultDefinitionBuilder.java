package org.fenrir.yggdrasil.core.definition;

import java.net.URISyntaxException;
import java.net.URL;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140315
 */
public class DefaultDefinitionBuilder
{
    private final Logger log = LoggerFactory.getLogger(DefaultDefinitionBuilder.class);
    
    private Unmarshaller unmarshaller;
    
    public DefaultDefinitionBuilder() throws ApplicationException
    {
        try{
            JAXBContext context = JAXBContext.newInstance(PluginDefinition.class, ApplicationDefinition.class);
            unmarshaller = context.createUnmarshaller();
        }
        catch(JAXBException e){
            log.error("Error creant unmarshaller: {}", e.getMessage(), e);
            throw new ApplicationException("Error llegint definició de l'aplicació", e);
        }				
    }
    
    public <T extends PluginDefinition> T buildDefinition(URL fileUrl) throws JAXBException, URISyntaxException
    {
    	return (T)unmarshaller.unmarshal(fileUrl);
    }    
}
