package org.fenrir.yggdrasil.core.descriptor;

import java.util.concurrent.ScheduledExecutorService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20131203
 */
public class RunnableTaskDescriptor 
{
    private Runnable task;
    private ScheduledExecutorService scheduler;
    private boolean active;
    private int initialDelay;
    private int delay;

    public RunnableTaskDescriptor(Runnable task, int initialDelay)
    {
        this(task, initialDelay, 0);
    }
    
    public RunnableTaskDescriptor(Runnable task, int initialDelay, int delay)
    {
        this.task = task;
        this.initialDelay = initialDelay;
        this.delay = delay;
        this.active = true;
    }

    public boolean isPeriodicTask()
    {
        return delay>0;
    }
    
    public Runnable getTask()
    {
        return task;
    }

    public boolean isTaskScheduled()
    {
        return scheduler!=null;
    }

    public ScheduledExecutorService getScheduler()
    {
        return scheduler;
    }

    public void setScheduler(ScheduledExecutorService scheduler)
    {
        this.scheduler = scheduler;
    }
    
    public boolean isActive()
    {
        return active;
    }
    
    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getInitialDelay()
    {
        return initialDelay;
    }

    public int getDelay()
    {
        return delay;
    }
}
