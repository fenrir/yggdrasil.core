package org.fenrir.yggdrasil.core.descriptor;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20121211
 */
@XmlRootElement(name="actions")
@XmlAccessorType(XmlAccessType.FIELD)
public class ArtifactUpdateActions 
{
    @XmlElement(name="updatable", defaultValue="false")
    protected boolean updatable;
    
    @XmlElement(name="action")
    protected List<ArtifactUpdateDescriptor> actions;
    
    public boolean isUpdatable()
    {
        return updatable;
    }
    
    public void setUpdatable(boolean updatable)
    {
        this.updatable = updatable;
    }
    
    public List<ArtifactUpdateDescriptor> getActions()
    {
        return actions;
    }
    
    public void setActions(List<ArtifactUpdateDescriptor> actions)
    {
        this.actions = actions;
    }
}
