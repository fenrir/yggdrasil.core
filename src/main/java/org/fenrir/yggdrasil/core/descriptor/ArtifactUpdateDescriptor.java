package org.fenrir.yggdrasil.core.descriptor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20121211
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ArtifactUpdateDescriptor 
{
    public static final String TYPE_CREATE_ARTIFACT = "CREATE";
    public static final String TYPE_UPDATE_ARTIFACT = "UPDATE";
    public static final String TYPE_DELETE_ARTIFACT = "DELETE";
    
    @XmlAttribute
    private String id;
    @XmlElement
    private String type;
    @XmlElement(name = "source-path")
    private String sourcePath;
    @XmlElement(name = "destination-path")
    private String destinationPath;
    @XmlElement
    private String url;

    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getType() 
    {
        return type;
    }

    public void setType(String type) 
    {
        this.type = type;
    }

    public String getSourcePath() 
    {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) 
    {
        this.sourcePath = sourcePath;
    }
    
    public String getDestinationPath() 
    {
        return destinationPath;
    }

    public void setDestinationPath(String destinationPath)
    {
        this.destinationPath = destinationPath;
    }

    public String getUrl() 
    {
        return url;
    }

    public void setUrl(String url) 
    {
        this.url = url;
    }
}
