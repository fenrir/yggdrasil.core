package org.fenrir.yggdrasil.core.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.matcher.AbstractMatcher;
import com.google.inject.name.Names;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.service.IApplicationUpdateService;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.yggdrasil.core.service.ITaskLauncherService;
import org.fenrir.yggdrasil.core.service.IWorkspaceAdministrationService;
import org.fenrir.yggdrasil.core.service.impl.ApplicationUpdateServiceImpl;
import org.fenrir.yggdrasil.core.service.impl.EventNotificationServiceImpl;
import org.fenrir.yggdrasil.core.service.impl.PreferenceServiceImpl;
import org.fenrir.yggdrasil.core.service.impl.TaskLauncherServiceImpl;
import org.fenrir.yggdrasil.core.service.impl.WorkspaceAdministrationServiceImpl;
import org.fenrir.yggdrasil.core.event.IApplicationNotification;
import org.fenrir.yggdrasil.core.event.NotificationEventConstants;
import org.fenrir.yggdrasil.core.event.SimpleNotification;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140315
 */
public class CoreStartupModule extends AbstractModule
{
	public static final String ID = "org.fenrir.yggdrasil.core.coreStartup";
	
	private final Logger log = LoggerFactory.getLogger(CoreStartupModule.class);
    
    @Override
    protected void configure()
    {
    	/* Notificacions */
        bind(IApplicationNotification.class)
                .annotatedWith(Names.named(NotificationEventConstants.NOTIFICATION_SIMPLE))
                .to(SimpleNotification.class);
        /* Serveis */
        bind(IEventNotificationService.class).to(EventNotificationServiceImpl.class).in(Singleton.class);
        bind(IPreferenceService.class).to(PreferenceServiceImpl.class).in(Singleton.class);
        bind(IWorkspaceAdministrationService.class).to(WorkspaceAdministrationServiceImpl.class).in(Singleton.class);
        bind(ITaskLauncherService.class).to(TaskLauncherServiceImpl.class).in(Singleton.class);
        bind(IApplicationUpdateService.class).to(ApplicationUpdateServiceImpl.class).in(Singleton.class);
        // Listener per inicialitzar els listeners després de la seva injecció
        CoreStartupModule.ObserverTypeListener typeListener = new CoreStartupModule.ObserverTypeListener();
        bindListener(new AbstractMatcher<TypeLiteral<?>>() 
        {
            @Override
            public boolean matches(TypeLiteral<?> typeLiteral) 
            {
                return typeLiteral.getRawType().getAnnotation(EventListener.class)!=null
                        || IEventNotificationService.class.isAssignableFrom(typeLiteral.getRawType());
            }
        }, typeListener);
    }
    
    class ObserverTypeListener implements TypeListener
    {        
        private IEventNotificationService eventNotificationService;
        private Map<Class, List<Object>> listenerMap = new HashMap<Class, List<Object>>();
        
        @Override
        public <I> void hear(final TypeLiteral<I> typeLiteral, TypeEncounter<I> typeEncounter) 
        {
            typeEncounter.register(new InjectionListener<I>() 
            {
                /**
                 * Mètode executat una vegada Guice ha injectat tots els membres a l'objecte
                 * S'han de tractar 2 casos: 1) S'està tractant el service de gestió d'events, cas
                 * en que es guardarà la referència a aquest per poder especificar els listeners que han de
                 * ser tractats i afegir els que ja s'han tractat anteriorment si n'hi ha.
                 * 2) S'està tractant un listener, cas en que es guardarà la seva referència si encara 
                 * no s'ha pogut recuperar el service o bé s'especificarà directament a aquest si ja s'ha pogut recuperar
                 * @param i I Objecte del que s'han injectat els membres.
                 */
                @Override
                public void afterInjection(I i) 
                {
                    // Service
                    if(IEventNotificationService.class.isAssignableFrom(i.getClass())){
                        if(log.isDebugEnabled()){
                            log.debug("Executant post inicialització del service de gestió d'events {}", i.getClass().getName());
                        }
                        eventNotificationService = (IEventNotificationService)i;
                        for(Class key:listenerMap.keySet()){
                            List<Object> listeners = listenerMap.get(key);
                            eventNotificationService.addListeners(key, listeners);
                        }
                    }
                    // Listener
                    else{
                        if(log.isDebugEnabled()){
                            log.debug("Executant post inicialització del listener {}", i.getClass().getName());
                        }
                        EventListener listenerAnnotation = i.getClass().getAnnotation(EventListener.class);
                        Class[] definitions = listenerAnnotation.definitions();
                        for(Class definition:definitions){
                            if(eventNotificationService!=null){
                                eventNotificationService.addListener(definition, i);
                            }
                            else{
                                List<Object> listeners = listenerMap.get(definition);
                                if(listeners==null){
                                    listeners = new ArrayList<Object>();
                                    listenerMap.put(definition, listeners);
                                }
                                listeners.add(i);
                            }
                        }                        
                    }
                }
            });
        }
    }
}
