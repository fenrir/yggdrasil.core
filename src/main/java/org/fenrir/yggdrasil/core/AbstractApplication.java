package org.fenrir.yggdrasil.core;

import java.net.URL;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.ServiceLoader;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.definition.ApplicationDefinition;
import org.fenrir.yggdrasil.core.definition.DefaultDefinitionBuilder;
import org.fenrir.yggdrasil.core.definition.ModuleDefinition;
import org.fenrir.yggdrasil.core.definition.PluginDefinition;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.event.IWorkspaceEventListener;
import org.fenrir.yggdrasil.core.module.CoreStartupModule;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.yggdrasil.core.service.ITaskLauncherService;
import org.fenrir.yggdrasil.core.service.IWorkspaceAdministrationService;
import org.fenrir.yggdrasil.core.task.UpdateApplicationTask;
import org.fenrir.yggdrasil.core.util.DefaultExceptionHandler;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.exception.PreferenceException;
import org.fenrir.yggdrasil.core.extension.ISplashScreenManagementService;
import org.fenrir.yggdrasil.core.extension.IWindowManagementService;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140315
 */
@EventListener(definitions=IWorkspaceEventListener.class)
public abstract class AbstractApplication implements IWorkspaceEventListener
{
	public static final String APPLICATION_DEFINITION_FILE = "application.xml";
	public static final String PLUGIN_DEFINITION_FILE = "plugin.xml";
	
    public static final int STARTUP_PROGRESS_INITIAL_VALUE = 0;
    public static final int STARTUP_PROGRESS_LOADING_CONTEXT = 25;
    public static final int STARTUP_PROGRESS_INITIALIZING_SERVICES = 50;
    public static final int STARTUP_PROGRESS_INITIALIZING_UI = 75;
    public static final int STARTUP_PROGRESS_COMPLETED = 100;
    
    private final Logger log = LoggerFactory.getLogger(AbstractApplication.class);
    
    private ISplashScreenManagementService splashScreenManager;
    private IWindowManagementService windowManager;
    
    private boolean initialized = false;

    public final void initialize() throws ApplicationException
    {
    	ServiceLoader<ISplashScreenManagementService> splashScreenManagerLoader = ServiceLoader.load(ISplashScreenManagementService.class);
    	Iterator<ISplashScreenManagementService> splashScreenManagerIterator = splashScreenManagerLoader.iterator();
    	// Si hi ha més d'una instancia declara, s'utilitza la primera que es troba
    	if(splashScreenManagerIterator.hasNext()){
    		splashScreenManager = splashScreenManagerIterator.next();
    		log.info("Trobat un proveidor de servei de gestió d'Splash Screen: {}", splashScreenManager.getClass().getName());
    	}
    	else{
    		log.info("No s'ha trobat cap proveidor de servei de gestió d'Splash Screen");
    	}
    	
    	ServiceLoader<IWindowManagementService> windowManagerLoader = ServiceLoader.load(IWindowManagementService.class);
    	Iterator<IWindowManagementService> windowManagerIterator = windowManagerLoader.iterator();
    	// Si hi ha més d'una instancia declara, s'utilitza la primera que es troba
    	if(windowManagerIterator.hasNext()){
    		windowManager = windowManagerIterator.next();
    		// Error derivat de YGG-02
    		log.info("Trobat un proveidor de servei de gestió d'UI: {}", windowManager.getClass().getName());
    	}
    	else{
    		log.error("No s'ha trobat cap proveidor de servei de gestió d'UI");
    		throw new ApplicationException("No s'ha trobat cap proveidor de servei de gestió d'UI");
    	}

    	if(splashScreenManager!=null){
    		splashScreenManager.drawSplashScreenProgress(STARTUP_PROGRESS_INITIAL_VALUE, "Inicialitzant aplicació...");
    	}
        
        // Es configura el gestor d'errors per defecte per tal de controlar els errors inesperats
        DefaultExceptionHandler exceptionHandler = new DefaultExceptionHandler();
        exceptionHandler.addErrorNotificationListener(new DefaultExceptionHandler.IErrorNotificationListener() 
        {
            @Override
            public void errorNotified(String message, Throwable error) 
            {
            	try{
            		windowManager.displayErrorMessage(message, error);
            	}
            	catch(Exception e){
            		log.warn("Error obtenint la referència del manager de la finestra principal: {}", e.getMessage(), e);
            		windowManager.displayStandaloneErrorMessage(message, error);
            	}
            }
        });
        Thread.setDefaultUncaughtExceptionHandler(exceptionHandler);
                
        if(splashScreenManager!=null){
    		splashScreenManager.drawSplashScreenProgress(STARTUP_PROGRESS_LOADING_CONTEXT, "Carregant contexte...");
        }

        ApplicationContext applicationContext = ApplicationContext.getInstance();
        try{
	        // Es recuperen les definicions de l'aplicació / plugins
	        Enumeration<URL> applicationFiles = this.getClass().getClassLoader().getResources(APPLICATION_DEFINITION_FILE);
	        if(!applicationFiles.hasMoreElements()){
	        	throw new ApplicationException("No s'ha trobat el fitxer de definició de l'aplicació");
	        }
	        URL applicationFileUrl = applicationFiles.nextElement();
	        if(applicationFiles.hasMoreElements()){
	        	throw new ApplicationException("S'han trobat més d'un fitxer de definició per l'aplicació");
	        }
	        DefaultDefinitionBuilder builder = new DefaultDefinitionBuilder();
	        ApplicationDefinition applicationDefinition = builder.buildDefinition(applicationFileUrl);
	        applicationContext.addDefinition(applicationDefinition);
	        
	        Enumeration<URL> pluginFiles = this.getClass().getClassLoader().getResources(PLUGIN_DEFINITION_FILE);
	        while(pluginFiles.hasMoreElements()){
	        	URL pluginFileURL = pluginFiles.nextElement();
	        	PluginDefinition pluginDefinition = builder.buildDefinition(pluginFileURL);
	        	applicationContext.addDefinition(pluginDefinition);
	        }
        }
        catch(Exception e){
        	log.error("Error recuperant la definició de l'aplicació: {}", e.getMessage(), e);
        	throw new ApplicationException("Error recuperant la definició de l'aplicació: " + e.getMessage(), e);
        }
        // Inicialització dels serveis bàsics
        ModuleDefinition moduleDefinition = new ModuleDefinition();
        moduleDefinition.setId(CoreStartupModule.ID);
        moduleDefinition.setClassName(CoreStartupModule.class.getName());
        moduleDefinition.setExecutionContext(CoreConstants.EXECUTION_CONTEXT_STARTUP);
        applicationContext.getPluginRegistry().addModuleDefinition(moduleDefinition);
        applicationContext.configureContext(CoreConstants.EXECUTION_CONTEXT_STARTUP);
        
        // S'especifica directament com a listener perquè la classe Application no està gestionada per Guice
        IEventNotificationService eventNotificationService = (IEventNotificationService)applicationContext.getRegisteredComponent(IEventNotificationService.class);
        eventNotificationService.addListener(IWorkspaceEventListener.class, this);
        
        initialized = true;
    }

    public final void run() throws ApplicationException
    {
        if(!initialized){
            throw new ApplicationException("Aplicació no inicialitzada");
        }
        
        if(splashScreenManager!=null){
    		splashScreenManager.drawSplashScreenProgress(STARTUP_PROGRESS_INITIALIZING_SERVICES, "Inicialitzant serveis...");
        }

        ApplicationContext applicationContext = ApplicationContext.getInstance();
        try{
            IWorkspaceAdministrationService workspaceService = (IWorkspaceAdministrationService)applicationContext.getRegisteredComponent(IWorkspaceAdministrationService.class);
            workspaceService.loadDefaultWorkspace();
        }
        catch(PreferenceException e){   
            // En cas d'error s'amaga l'splash screen
        	if(splashScreenManager!=null){
        		splashScreenManager.closeSplashScreen();
        	}
            throw new ApplicationException("Error al carregar el workspace per defecte: " + e.getMessage());
        }
        
        try{
            start();
        }
        catch(ApplicationException e){
            // En cas d'error s'amaga l'splash screen
        	if(splashScreenManager!=null){
        		splashScreenManager.closeSplashScreen();
        	}
            throw e;
        }

        if(splashScreenManager!=null){
    		splashScreenManager.drawSplashScreenProgress(STARTUP_PROGRESS_INITIALIZING_UI, "Inicialitzant UI...");
        }
        
        // Es crea la finestra de l'aplicació
        windowManager.initialize(this);
        windowManager.notifyPreWindowOpenEvent();
        try{
            windowManager.createWindow();
        }
        catch(Exception e){
            log.error("Error fatal inicialitzant la UI de l'aplicació: " + e.getMessage(), e);
            // En cas d'error s'amaga l'splash screen
            if(splashScreenManager!=null){
        		splashScreenManager.closeSplashScreen();
            }
            throw new ApplicationException("Error fatal inicialitzant la UI de l'aplicació: " + e.getMessage(), e);
        }

        if(splashScreenManager!=null){
    		splashScreenManager.drawSplashScreenProgress(STARTUP_PROGRESS_COMPLETED, "Inicialitzanció completada");
    		// Abans d'obrir la finestra principal s'amaga l'splash screen
    		splashScreenManager.closeSplashScreen();
    		// S'elimina la referència perquè es pugui fer neteja de l'objecte
    		splashScreenManager = null;
        }
        
        windowManager.showWindow();
    }    

    /**
     * Mètode que realitza les inicialitzacións PRE obertura de la finestra principal de l'aplicació
     * @throws ApplicationException 
     */
    public abstract void start() throws ApplicationException;
    public abstract void stop() throws ApplicationException;
    
    @Override
    public final void beforeWorkspaceLoad()
    {
        onBeforeWorkspaceLoad();
    }
    
    public void onBeforeWorkspaceLoad()
    {
        
    }
    
    @Override
    public final void workspaceLoaded() throws ApplicationException
    {
        if(log.isDebugEnabled()){
            log.debug("Configurant contexte del workspace");
        }
        
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        IPreferenceService preferenceService = (IPreferenceService)applicationContext.getRegisteredComponent(IPreferenceService.class);

        /* Configuració del proxy */
        String httpProxyHost = preferenceService.getProperty(CoreConstants.PREFERENCES_PROXY_HOST);
        if(StringUtils.isNotBlank(httpProxyHost)){
            String httpProxyPort = preferenceService.getProperty(CoreConstants.PREFERENCES_PROXY_PORT, CoreConstants.PREFERENCES_HTTP_PROXY_PORT_DEFAULT);
            System.setProperty("http.proxyHost", httpProxyHost);
            System.setProperty("http.proxyPort", httpProxyPort);
        }

        /* Es programen les tasques automàtiques configurades en inicialitzar l'aplicació
         * TODO Fer possible el llançament de la tasca sense workspace carregat. Moure la configuració a application.xml / plugin.xml 
         */
        ITaskLauncherService taskLauncherService = (ITaskLauncherService)applicationContext.getRegisteredComponent(ITaskLauncherService.class);
        taskLauncherService.initializeTasks();
        // Es programa la tasca automàtica per comprobar les actualitzacions
        taskLauncherService.scheduleTask(UpdateApplicationTask.ID);
        
        onWorkspaceLoaded();        
    }
    
    public void onWorkspaceLoaded() throws ApplicationException
    {
        
    }
 
    @Override
    public final void beforeWorkspaceUnload() throws ApplicationException
    {
        if(log.isDebugEnabled()){
            log.debug("Executant aturada del contexte del workspace");
        }
        
        onBeforeWorkspaceUnload();
        
        ITaskLauncherService taskLauncherService = (ITaskLauncherService)ApplicationContext.getInstance().getRegisteredComponent(ITaskLauncherService.class);
        taskLauncherService.shutdownAllTasks();
    }
    
    public void onBeforeWorkspaceUnload() throws ApplicationException
    {
        
    }
    
    @Override
    public final void workspaceUnloaded() throws ApplicationException
    {
        onWorkspaceUnloaded();
    }
    
    public void onWorkspaceUnloaded() throws ApplicationException
    {
        
    }
}
