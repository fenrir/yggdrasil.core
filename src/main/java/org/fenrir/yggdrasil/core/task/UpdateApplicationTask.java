package org.fenrir.yggdrasil.core.task;

import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.descriptor.ArtifactUpdateDescriptor;
import org.fenrir.yggdrasil.core.event.IApplicationNotification;
import org.fenrir.yggdrasil.core.event.IApplicationNotificationListener;
import org.fenrir.yggdrasil.core.event.SimpleNotification;
import org.fenrir.yggdrasil.core.event.NotificationEventConstants;
import org.fenrir.yggdrasil.core.service.IApplicationUpdateService;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140609
 */
public class UpdateApplicationTask implements Runnable
{
    public static final String ID = "org.fenrir.yggdrasil.core.task.updateApplicationTask";
    
    private final Logger log = LoggerFactory.getLogger(UpdateApplicationTask.class);
        
    @Inject 
    private IApplicationUpdateService updateService;
    
    @Inject
    private IEventNotificationService eventNotificationService;
    
    public void setUpdateService(IApplicationUpdateService updateService)
    {
        this.updateService = updateService;
    }
    
    public void setEventNotificationService(IEventNotificationService eventNotificationService)
    {
        this.eventNotificationService = eventNotificationService;
    }
    
    @Override
    public void run()
    {
        if(log.isDebugEnabled()){
            log.debug("Iniciada tasca de comprobació d'actualitzacións de l'aplicació");
        }                                

        try{
            updateService.prepareUpdateFolder();
            Map<String, String> updates = updateService.checkUpdateVersion();
            // En el cas que hi hagi 2 entrades voldrà dir que hi ha una versió de la que s'ha de descarregar el distribuible
            if(updates.size()>1){
                if(log.isDebugEnabled()){
                    log.debug("S'ha trobat una nova versió de l'aplicació disponible per descarregar");
                }

                try{
                	// TODO Crear notificació que permeti la descàrrega de versions completes
                    // No cal assignar-li ID perquè serà una notificació de tipus únic
                    SimpleNotification notification = new SimpleNotification();
                    notification.setCaption("Tasca de comprobació d'actualitzacions");
                    notification.setMessage("Hi ha disponible una nova versió de l'aplicació per descarregar");                    
                    notification.setIconPath("/org/fenrir/yggdrasil/ui/icons/upgrade_22.png");                    
                    eventNotificationService.notifyNamedEvent(IApplicationNotificationListener.class, 
                            IApplicationNotificationListener.EVENT_SINGLE_NOTIFICATION_ID, notification);                
                }
                catch(Exception e){
                    log.error("Error reportant notificació: {}", e.getMessage(), e);
                    // TODO Mostrar error per pantalla
                }  
            }
            // En el cas que es pugui actualitzar directament, es comproven les actualitzacions dels artifacts
            else{
	            Map.Entry<String, String> artifactsEntry = (Map.Entry<String, String>)updates.entrySet().toArray()[0];
	            List<ArtifactUpdateDescriptor> artifactDescriptors = updateService.checkArtifacts(artifactsEntry.getKey(), artifactsEntry.getValue());
	            if(!artifactDescriptors.isEmpty()){
	                if(log.isDebugEnabled()){
	                    log.debug("S'han trobat {} components per actualitzar", artifactDescriptors.size());
	                }
	                
	                updateService.createActionsFile(artifactDescriptors);
	
	                IApplicationNotification notification = (IApplicationNotification)ApplicationContext.getInstance().getRegisteredComponent(IApplicationNotification.class, NotificationEventConstants.NOTIFICATION_APPLICATION_UPDATED);
	                eventNotificationService.notifyNamedEvent(IApplicationNotificationListener.class, 
	                        IApplicationNotificationListener.EVENT_SINGLE_NOTIFICATION_ID, notification);                
	            }                        
	            else{
	                if(log.isDebugEnabled()){
	                    log.debug("No s'han trobat noves actualitzacions per l'aplicació");
	                }
	            }
            }
        }
        catch(Exception e){
            log.error("Error buscant actualitzacions de l'aplicació: {}", e.getMessage(), e);
            
            IApplicationNotification notification = (IApplicationNotification)ApplicationContext.getInstance().getRegisteredComponent(IApplicationNotification.class, NotificationEventConstants.NOTIFICATION_ERROR);
            notification.setMessage("Error a l'execució de la tasca d'actualització de l'aplicació");
            notification.setParameter(NotificationEventConstants.NOTIFICATION_PARAMETER_ERROR_DESCRIPTION, "Error a l'execució de la tasca d'actualització de l'aplicació");
            notification.setParameter(NotificationEventConstants.NOTIFICATION_PARAMETER_ERROR_THROWABLE, e);
            try{
                eventNotificationService.notifyNamedEvent(IApplicationNotificationListener.class, 
                        IApplicationNotificationListener.EVENT_SINGLE_NOTIFICATION_ID, notification);
            }
            catch(Exception e2){
                log.error("Error reportant notificació: {}", e2.getMessage(), e2);                
            }
        }        
        
        if(log.isDebugEnabled()){
            log.debug("Finalitzada tasca de comprobació d'actualitzacións de l'aplicació");
        }                
    }
}    
