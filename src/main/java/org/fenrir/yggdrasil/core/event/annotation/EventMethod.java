package org.fenrir.yggdrasil.core.event.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * TODO 1.0 javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20131202
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface EventMethod 
{
    String eventName();
}
