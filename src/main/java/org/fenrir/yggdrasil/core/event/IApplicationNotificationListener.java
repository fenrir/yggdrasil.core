package org.fenrir.yggdrasil.core.event;

import org.fenrir.yggdrasil.core.event.annotation.EventMethod;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.3.20131202
 */
public interface IApplicationNotificationListener 
{
    public static final String EVENT_SINGLE_NOTIFICATION_ID = "org.fenrir.yggdrasil.core.event.singleNotification";
    
    @EventMethod(eventName=EVENT_SINGLE_NOTIFICATION_ID)
    public void singleNotification(IApplicationNotification notification);
}
