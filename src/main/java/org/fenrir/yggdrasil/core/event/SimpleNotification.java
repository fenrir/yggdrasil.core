package org.fenrir.yggdrasil.core.event;

import java.util.UUID;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131202
 */
public class SimpleNotification implements IApplicationNotification
{
    private String id;
    private String caption;
    private String message;    
    private String iconPath;
    
    @Override
    public String getId()
    {
        if(id==null){
            id = UUID.randomUUID().toString();  
        }
        
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    @Override
    public String getCaption() 
    {
        return caption;
    }
    
    @Override
    public void setCaption(String caption)
    {
        this.caption = caption;
    }

    @Override
    public String getMessage() 
    {
        return message;
    }
    
    @Override
    public void setMessage(String message)
    {
        this.message = message;
    }

    @Override
    public String getIconPath() 
    {
        return iconPath;
    }    
    
    @Override
    public void setIconPath(String iconPath)
    {
        this.iconPath = iconPath;
    }
    
    @Override
    public final void setParameter(String id, Object value)
    {
        
    }
}
