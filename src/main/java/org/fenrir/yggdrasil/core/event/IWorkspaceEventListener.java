package org.fenrir.yggdrasil.core.event;

import org.fenrir.yggdrasil.core.event.annotation.EventMethod;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.3.20131202
 */
public interface IWorkspaceEventListener 
{
    public static final String EVENT_BEFORE_WORKSPACE_LOAD_ID = "org.fenrir.yggdrasil.core.event.workspace.beforeLoad";
    public static final String EVENT_WORKSPACE_LOADED_ID = "org.fenrir.yggdrasil.core.event.workspace.loaded";
    public static final String EVENT_BEFORE_WORKSPACE_UNLOAD_ID = "org.fenrir.yggdrasil.core.event.workspace.beforeUnload";
    public static final String EVENT_WORKSPACE_UNLOADED_ID = "org.fenrir.yggdrasil.core.event.workspace.unloaded";
    
    /**
     * Mètode cridat just abans de carregar el workspace. 
     * En aquest moment el context relatiu al workspace encara no ha estat carregat
     * pel que només està disponible el bàsic
     * @throws ApplicationException - Si es produeix un error en tractar l'event 
     */
    @EventMethod(eventName=EVENT_BEFORE_WORKSPACE_LOAD_ID)
    public void beforeWorkspaceLoad() throws ApplicationException;
    
    /**
     * Mètode cridat després de carregar el workspace. 
     * En aquest moment el context relatiu al workspace ja ha estat carregat i disponible
     * @throws ApplicationException - Si es produeix un error en tractar l'event 
     */
    @EventMethod(eventName=EVENT_WORKSPACE_LOADED_ID)
    public void workspaceLoaded() throws ApplicationException;
    
    /**
     * Mètode cridat just abans d'alliberar el workspace. 
     * En aquest moment el context relatiu al workspace encara està carregat i disponible
     * @throws ApplicationException - Si es produeix un error en tractar l'event 
     */
    @EventMethod(eventName=EVENT_BEFORE_WORKSPACE_UNLOAD_ID)
    public void beforeWorkspaceUnload() throws ApplicationException;
    
    
    /**
     * Mètode cridat just abans de carregar el workspace. 
     * En aquest moment el context relatiu al workspace ja ha estat alliberat
     * pel que només està disponible el bàsic
     * @throws ApplicationException - Si es produeix un error en tractar l'event 
     */
    @EventMethod(eventName=EVENT_WORKSPACE_UNLOADED_ID)
    public void workspaceUnloaded() throws ApplicationException;
}