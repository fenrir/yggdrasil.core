package org.fenrir.yggdrasil.core.event;

import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.3.20131202
 */
public class WorkspaceEventAdapter implements IWorkspaceEventListener
{

    @Override
    public void beforeWorkspaceLoad() throws ApplicationException 
    {
        
    }

    @Override
    public void workspaceLoaded() throws ApplicationException 
    {
        
    }

    @Override
    public void beforeWorkspaceUnload() throws ApplicationException 
    {
        
    }

    @Override
    public void workspaceUnloaded() throws ApplicationException 
    {

    }
}
