package org.fenrir.yggdrasil.core.event.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Anotació que indica que una classe actua com a listener d'events de l'aplicació.
 * Per tal d'evitar problemes d'herència múltiple, aquesta anotació sempre anirà a 
 * la classe implementadora de l'event, evitant l'interfície definitoria de l'event.
 * En cas que es fes de l'altra manera, seria possible que una classe implementés
 * varies interficies anotades amb anotacions que tinguessin diferents atributs. 
 * En el futur es podria implementar una utilitat per buscar aquesta anotació dins 
 * de les superclasses i interficies definides a cada classe que es vulgui comprobar
 * per poder fer que concretament aquesta si es pugui "heredar"
 * @author Antonio Archilla Nava
 * @version v0.3.20131202
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface EventListener 
{
    Class[] definitions();
}
