package org.fenrir.yggdrasil.core.event;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131202
 */
public interface IApplicationNotification 
{
    public String getId();
    public String getCaption();
    public void setCaption(String caption);
    public String getMessage();
    public void setMessage(String message);    
    public String getIconPath();
    public void setIconPath(String iconPath);
    public void setParameter(String id, Object value);
}
