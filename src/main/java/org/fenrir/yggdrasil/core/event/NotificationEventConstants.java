package org.fenrir.yggdrasil.core.event;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.1.20131202
 */
public class NotificationEventConstants 
{
    public static final String NOTIFICATION_SIMPLE = "org.fenrir.yggdrasil.core.notification.simple";
    public static final String NOTIFICATION_ERROR = "org.fenrir.yggdrasil.core.notification.error";
    public static final String NOTIFICATION_APPLICATION_UPDATED = "org.fenrir.yggdrasil.core.notification.applicationUpdated";
//    public static final String NOTIFICATION_ISSUES_UPDATED = "org.fenrir.dragonfly.core.notification.issuesUpdated";
    
    /* Paràmetres de les notificacions */
    public static final String NOTIFICATION_PARAMETER_ERROR_DESCRIPTION = "error_description";
    public static final String NOTIFICATION_PARAMETER_ERROR_THROWABLE = "error_throwable";
}
