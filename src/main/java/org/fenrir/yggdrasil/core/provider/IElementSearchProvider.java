package org.fenrir.yggdrasil.core.provider;

import java.util.List;
import org.fenrir.yggdrasil.core.exception.ProviderException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140108
 */
public interface IElementSearchProvider<T> 
{
	public List<T> findAllElements() throws ProviderException;
	public List<T> findElementsNameLike(String name) throws ProviderException;
}
