package org.fenrir.yggdrasil.core;

import java.util.ArrayList;
import java.util.List;
import com.google.inject.Binding;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Module;
import com.google.inject.name.Names;
import org.apache.commons.collections.MapIterator;
import org.apache.commons.collections.map.LinkedMap;
import org.apache.onami.lifecycle.jsr250.PostConstructModule;
import org.apache.onami.lifecycle.jsr250.PreDestroyModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.definition.ApplicationDefinition;
import org.fenrir.yggdrasil.core.definition.ModuleDefinition;
import org.fenrir.yggdrasil.core.definition.PluginDefinition;
import org.fenrir.yggdrasil.core.definition.PluginDefinitionRegistry;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO v1.0 Documentació
 * TODO Permetre contextes disjunts
 * @author Antonio Archilla Nava
 * @version v0.1.20141229
 */
public class ApplicationContext
{
	private final Logger log = LoggerFactory.getLogger(ApplicationContext.class);
	
    private static ApplicationContext instance;

    private PluginDefinitionRegistry pluginRegistry = new PluginDefinitionRegistry();
    
    private LinkedMap contextInjectors = new LinkedMap();
    private Injector currentInjector;
    
    private ApplicationContext()
    {
    	
    }

    public static ApplicationContext getInstance()
    {
        if(instance==null){
            instance = new ApplicationContext();
        }

        return instance;
    }
    
    public PluginDefinitionRegistry getPluginRegistry()
    {
    	return pluginRegistry;
    }
    
    void addDefinition(PluginDefinition definition)
    {
    	if(definition instanceof ApplicationDefinition){
    		pluginRegistry.setProductDefinition(((ApplicationDefinition) definition).getProduct());
    		if(((ApplicationDefinition)definition).getArtifacts()!=null){
    			pluginRegistry.setArtifactsDefinition(((ApplicationDefinition) definition).getArtifacts());
    		}
    	}
    	if(definition.getPreferences()!=null){
    		pluginRegistry.addPreferencesDefinition(definition.getPreferences());
    	}
    	pluginRegistry.addModuleDefinitions(definition.getModules());
    }
    
    public void configureContext(String executionContext) throws ApplicationException
    {
    	if(contextInjectors.containsKey(executionContext)){
    		throw new IllegalArgumentException("Operació no permesa; El context ja es troba carregat");
    	}
    	
    	List<Module> injectorModules = new ArrayList<Module>();
    	for(ModuleDefinition moduleDef:pluginRegistry.getModuleDefinitions()){
    		if(executionContext.equals(moduleDef.getExecutionContext())){
    			try{
        			Class<? extends Module> moduleClass = (Class<? extends Module>)Class.forName(moduleDef.getClassName());
        			Module module = (Module)moduleClass.getConstructor().newInstance(null);
    				injectorModules.add(module);        			
        		}
    			catch(Exception e){
        			log.error("No s'ha pogut crear el mòdul ${}: ${}", new Object[]{moduleDef.getId(), e.getMessage(), e});
        			throw new ApplicationException("No s'ha pogut crear el mòdul amb nom " + moduleDef.getId(), e);
        		}
    		}
    	}

    	// Si no hi ha mòduls configurats pel contexte especificat no es fa res
    	if(injectorModules.isEmpty()){
    		return;
    	}
    	
    	if(currentInjector==null){
    		// Add support to JSR-250 lifecycle annotations
    		injectorModules.add(new PostConstructModule());
    		injectorModules.add(new PreDestroyModule());
    		currentInjector = Guice.createInjector(injectorModules);    		
    	}
    	else{
    		currentInjector = currentInjector.createChildInjector(injectorModules);
    	}
    	contextInjectors.put(executionContext, currentInjector);
    }
    
    public void disposeContext(String executionContext)
    {
        if(!contextInjectors.containsKey(executionContext)){
            throw new IllegalArgumentException("Operació no permesa; El context no està carregat");
        }
        
        boolean remove = false;
        Injector parentInjector = null;
        MapIterator iterator = contextInjectors.mapIterator();
        while(iterator.hasNext()){
        	Object key = iterator.next();
        	if(executionContext.equals(key)){
        		remove = true;
        	}
        	if(remove){
        		iterator.remove();
        	}
        	else{
        		parentInjector = (Injector)iterator.getValue();
        	}
        }        
        
        currentInjector = parentInjector;
    }
     
    public Object getRegisteredComponent(Key<?> key)
    {
    	return currentInjector.getInstance(key);    	
    }
    
    public <T> T getRegisteredComponent(Class<T> type)
    {
    	return currentInjector.getInstance(type);    	
    }
    
    public <T> T getRegisteredComponent(Class<T> type, String name)
    {
        Key<T> key = Key.get(type, Names.named(name));
        return currentInjector.getInstance(key);        
    }
    
    public <T> T getOptionalRegisteredComponent(Class<T> type)
    {
    	Key<T> key = Key.get(type);
    	return getOptionalRegisteredComponent(key);
    }
    
    public <T> T getOptionalRegisteredComponent(Class<T> type, String name)
    {
    	Key<T> key = Key.get(type, Names.named(name));
    	return getOptionalRegisteredComponent(key);
    }
    
    private <T> T getOptionalRegisteredComponent(Key<T> key)
    {
    	Binding<T> binding = currentInjector.getExistingBinding(key);
    	if(binding==null){
    		return null;
    	}
    	return currentInjector.getProvider(key).get();
    }

    public void injectMembers(Object object)
    {
    	currentInjector.injectMembers(object);    	
    }
}