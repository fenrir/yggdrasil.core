package org.fenrir.yggdrasil.core.extension;

import org.fenrir.yggdrasil.core.AbstractApplication;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20130315
 */
public interface IWindowManagementService 
{
	public void initialize(AbstractApplication application);
	
	public void notifyPreWindowOpenEvent();
	
	public void displayErrorMessage(String message, Throwable error);
	public void displayStandaloneErrorMessage(String message, Throwable error);
	
	public void createWindow() throws ApplicationException;
	public void showWindow();
}
