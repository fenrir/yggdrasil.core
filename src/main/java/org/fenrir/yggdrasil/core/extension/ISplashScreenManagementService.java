package org.fenrir.yggdrasil.core.extension;

public interface ISplashScreenManagementService 
{
	public void drawSplashScreenProgress(int progress, String message);
	public void closeSplashScreen();
}
