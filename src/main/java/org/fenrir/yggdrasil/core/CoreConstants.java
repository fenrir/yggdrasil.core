package org.fenrir.yggdrasil.core;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20141011
 */
public class CoreConstants 
{
	/*--------------------------*
	 *    EXECUTION CONTEXTS    * 
	 *--------------------------*/
	public static final String EXECUTION_CONTEXT_STARTUP = "startup";
	public static final String EXECUTION_CONTEXT_WORKSPACE_INITIALIZED = "workspace_initialized";
	
	/*--------------*
	 * PREFERENCIES * 
	 *--------------*/
	public static final String PREFERENCES_WORKSPACE_IS_DEFAULT = "//workspaces/workspace[contains(folder, '${folder}')]/@default";
    public static final String PREFERENCES_WORKSPACE_DEFAULT_FOLDER = "//workspaces/workspace[@default]/folder";
    public static final String PREFERENCES_WORKSPACE_RESTART_FOLDER = "//workspaces/workspace[@restart]/folder";
    public static final String PREFERENCES_WORKSPACE_CURRENT = "//transient/workspace/folder";
    // Propietat VERSION de les preferències
    public static final String PREFERENCES_VERSION = "//preferences[@id='${id}']/@version";
    
    public static final String PREFERENCES_UPDATE_SITE = "//preferences/update/site";
    public static final String PREFERENCES_UPDATE_CHANNEL = "//preferences/update/channel";
    
    public static final String PREFERENCES_TASKS_NAMES = "//preferences/tasks/task/@name";
    public static final String PREFERENCES_TASK_CLASS = "//preferences/tasks/task[contains(@name, '${id}')]/class";
    public static final String PREFERENCES_TASK_ACTIVE = "//preferences/tasks/task[contains(@name, '${id}')]/active";
    public static final String PREFERENCES_TASK_INITIAL_DELAY = "//preferences/tasks/task[contains(@name, '${id}')]/initialDelay";
    public static final String PREFERENCES_TASK_DELAY = "//preferences/tasks/task[contains(@name, '${id}')]/delay";
    
    public static final String PREFERENCES_PROXY_HOST = "//preferences[@id='org.fenrir.yggdrasil.core']/proxy/host";
    public static final String PREFERENCES_PROXY_PORT = "//preferences[@id='org.fenrir.yggdrasil.core']/proxy/port";
    public static final String PREFERENCES_HTTP_PROXY_PORT_DEFAULT = "80";
    
    /*--------------*
	 *    UPDATE    * 
	 *--------------*/
    public static final String UPDATE_FOLDER = "update";
    public static final int UPDATE_DOWNLOAD_BUFFER_SIZE = 2048; 
    public static final String UPDATE_DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13";
    
    public static final String UPDATE_FILE = "update.xml";
    public static final String UPDATE_FILE_LOCATION = UPDATE_FOLDER + "/" + UPDATE_FILE;
    public static final String UPDATE_FILE_ARTIFACTS = "artifacts.xml";
    public static final String UPDATE_FILE_ARTIFACTS_LOCATION = UPDATE_FOLDER + "/" + UPDATE_FILE_ARTIFACTS;
    public static final String UPDATE_FILE_ACTIONS = "actions.xml";
    public static final String UPDATE_FILE_ACTIONS_LOCATION = UPDATE_FOLDER + "/" + UPDATE_FILE_ACTIONS;

    /* Expressions XPath */
    public static final String UPDATE_LAST_VERSION = "//channel[@id=''{0}'']/update[last()]/@version";
    public static final String UPDATE_MIN_VERSION = "//channel[@id=''{0}'']/update[@version=''{1}'']/minVersion";
    public static final String UPDATE_DIST_URL = "//channel[@id=''{0}'']/update[@version=''{1}'']/dist/url";
    public static final String UPDATE_ARTIFACTS_URL = "//channel[@id=''{0}'']/update[@version=''{1}'']/artifacts/url";
    
    public static final String UPDATE_APPLICATION_VERSION = "/artifacts/@version";
    public static final String UPDATE_ARTIFACT_IDS = "//artifact/@id";
    public static final String UPDATE_ARTIFACT_VERSION = "//artifact[@id=''{0}'']/version";
    public static final String UPDATE_ARTIFACT_PATH = "//artifact[@id=''{0}'']/path";
    public static final String UPDATE_ARTIFACT_URL = "//artifact[@id=''{0}'']/url";
}
