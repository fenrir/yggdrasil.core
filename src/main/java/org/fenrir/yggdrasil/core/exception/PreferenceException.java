package org.fenrir.yggdrasil.core.exception;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20131202
 */
@SuppressWarnings("serial")
public class PreferenceException extends Exception 
{
    public PreferenceException() 
    {
        super();	
    }

    public PreferenceException(String message, Throwable cause) 
    {
        super(message, cause);
    }

    public PreferenceException(String message) 
    {
        super(message);	
    }

    public PreferenceException(Throwable cause) 
    {
        super(cause);	
    }	
}