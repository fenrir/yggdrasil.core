package org.fenrir.yggdrasil.core.exception;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20131202
 */
@SuppressWarnings("serial")
public class ApplicationRuntimeException extends RuntimeException 
{
    public ApplicationRuntimeException() 
    {
        super();	
    }

    public ApplicationRuntimeException(String message, Throwable cause) 
    {
        super(message, cause);
    }

    public ApplicationRuntimeException(String message) 
    {
        super(message);	
    }

    public ApplicationRuntimeException(Throwable cause) 
    {
        super(cause);	
    }
}