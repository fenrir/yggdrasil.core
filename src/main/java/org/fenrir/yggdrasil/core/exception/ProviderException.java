package org.fenrir.yggdrasil.core.exception;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
@SuppressWarnings("serial")
public class ProviderException extends Exception 
{
    public ProviderException() 
    {
        super();	
    }

    public ProviderException(String message, Throwable cause) 
    {
        super(message, cause);
    }

    public ProviderException(String message) 
    {
        super(message);	
    }

    public ProviderException(Throwable cause) 
    {
        super(cause);	
    }
}