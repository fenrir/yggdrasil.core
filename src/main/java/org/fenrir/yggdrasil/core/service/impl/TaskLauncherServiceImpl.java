package org.fenrir.yggdrasil.core.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.CoreConstants;
import org.fenrir.yggdrasil.core.descriptor.RunnableTaskDescriptor;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.yggdrasil.core.service.ITaskLauncherService;

/**
 * TODO v1.0 Javadoc
 * TODO v0.2 Revisar els casos que la ID no està present al registre de tasques per fer schedule o shutdown
 * @author Antonio Archilla Nava
 * @version v0.3.20131226
 */
public class TaskLauncherServiceImpl implements ITaskLauncherService
{
    private final Logger log = LoggerFactory.getLogger(TaskLauncherServiceImpl.class);
    
    private Map<String, RunnableTaskDescriptor> tasks = new HashMap<String, RunnableTaskDescriptor>();
    
    @Inject
    private IPreferenceService preferenceService;
    
    public void setPreferenceService(IPreferenceService preferenceService)
    {
        this.preferenceService = preferenceService;
    }
    
    @Override
    public void initializeTasks() throws ApplicationException
    {
        
        List<String> taskIds = preferenceService.getProperties(CoreConstants.PREFERENCES_TASKS_NAMES);      
        for(String id:taskIds){
            Map<String, String> variables = new HashMap<String, String>();
            variables.put("id", id);
            String className = preferenceService.getProperty(CoreConstants.PREFERENCES_TASK_CLASS, variables);
            String active = preferenceService.getProperty(CoreConstants.PREFERENCES_TASK_ACTIVE, variables, "true");
            String initialDelay = preferenceService.getProperty(CoreConstants.PREFERENCES_TASK_INITIAL_DELAY, variables);
            // Si la tasca es d'una única execució el delay serà 0 encara que no s'hagi indicat
            String delay = preferenceService.getProperty(CoreConstants.PREFERENCES_TASK_DELAY, variables, "0");

            try{                
                Runnable task = (Runnable)Class.forName(className).getConstructor().newInstance(null);
                ApplicationContext.getInstance().injectMembers(task);
                
                RunnableTaskDescriptor descriptor = new RunnableTaskDescriptor(task, Integer.parseInt(initialDelay), Integer.parseInt(delay));
                descriptor.setActive("true".equals(active));
                tasks.put(id, descriptor);                 
            }
            catch(ClassNotFoundException e){
                log.error("No s'ha trobat la tasca amb nom ${}: ${}", new Object[]{id, e.getMessage(), e});
                throw new ApplicationException("No s'ha trobat la tasca amb nom " + id, e);
            }
            catch(InstantiationException e){
                log.error("No s'ha pogut crear la tasca amb nom ${}: ${}", new Object[]{id, e.getMessage(), e});
                throw new ApplicationException("No s'ha pogut crear la tasca amb nom " + id, e);
            }
            catch(Exception e){
                log.error("No s'ha pogut invocar la tasca amb nom ${}: ${}", new Object[]{className, e.getMessage(), e});
                throw new ApplicationException("No s'ha pogut invocar la tasca amb nom " + id, e);
            }
        }
    }    
    
    /**
     * TODO Tractar el cas que la tasca ja estigui programada
     * @param id 
     */
    @Override
    public void scheduleTask(String id)
    {
        RunnableTaskDescriptor descriptor = tasks.get(id);
        // Si no està activa no es fa res
        if(descriptor==null || !descriptor.isActive()){
            log.info("No es planifica tasca inactiva: {}", id);
            return;
        }
        
        log.info("Planificant tasca: {} amb execució {}/{} min.", 
                new String[]{id, Integer.toString(descriptor.getInitialDelay()), Integer.toString(descriptor.getDelay())});
        
        /* Es pograma la tasca per la seva execució cada X minuts. Si aquesta tarda
         * Si l'interval de temps entre crides es massa curt i es solapa amb el començament
         * de la següent, aquesta es veura endarrerida
         */
        // Es configura l'scheduler (1 thread)
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        // Tasques periodiques
        if(descriptor.isPeriodicTask()){
            scheduler.scheduleWithFixedDelay(descriptor.getTask(), descriptor.getInitialDelay(), descriptor.getDelay(), TimeUnit.MINUTES);
        }
        // Tasques d'una única execució
        else{
            scheduler.schedule(descriptor.getTask(), descriptor.getInitialDelay(), TimeUnit.MINUTES);
        }
        descriptor.setScheduler(scheduler);
    }

    /**
     * TODO Tractar el cas que la tasca ja estigui programada
     * @param id
     */
    @Override
    public void scheduleTask(String id, int initialDelay)
    {
        RunnableTaskDescriptor descriptor = tasks.get(id);
        // Si no està activa no es fa res
        if(descriptor==null || !descriptor.isActive()){
            log.info("No es planifica tasca inactiva: {}", id);
            return;
        }
        
        log.info("Planificant tasca: {} amb execució {}/{} min.", 
                new String[]{id, Integer.toString(descriptor.getInitialDelay()), Integer.toString(descriptor.getDelay())});
        
        /* Es pograma la tasca per la seva execució cada X minuts. Si aquesta tarda
         * Si l'interval de temps entre crides es massa curt i es solapa amb el començament
         * de la següent, aquesta es veura endarrerida
         */
        // Es configura l'scheduler (1 thread)
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        // Tasques periodiques
        if(descriptor.isPeriodicTask()){
            scheduler.scheduleWithFixedDelay(descriptor.getTask(), initialDelay, descriptor.getDelay(), TimeUnit.MINUTES);
        }
        // Tasques d'una única execució
        else{
            scheduler.schedule(descriptor.getTask(), initialDelay, TimeUnit.MINUTES);
        }
        descriptor.setScheduler(scheduler);
    }
    
    @Override
    public void shutdownTask(String id)
    {
        RunnableTaskDescriptor descriptor = tasks.get(id);
        if(descriptor!=null){
            descriptor.getScheduler().shutdown();
        }
    }
    
    @Override
    public void shutdownAllTasks()
    {
        Iterator<Entry<String, RunnableTaskDescriptor>> iterator = tasks.entrySet().iterator();
        while(iterator.hasNext()){
            RunnableTaskDescriptor descriptor = iterator.next().getValue();
            descriptor.getScheduler().shutdown();
        }
    }
    
    @Override
    public boolean isTaskScheduled(String id)
    {
        RunnableTaskDescriptor descriptor = tasks.get(id);
        // TODO Revisar comprobació
        if(descriptor!=null){
            return descriptor.isTaskScheduled();
        }
        
        return false;
    }
}
