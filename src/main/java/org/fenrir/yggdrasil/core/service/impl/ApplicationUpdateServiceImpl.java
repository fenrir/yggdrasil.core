package org.fenrir.yggdrasil.core.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.net.ProxySelector;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.impl.conn.ProxySelectorRoutePlanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.CoreConstants;
import org.fenrir.yggdrasil.core.descriptor.ArtifactUpdateActions;
import org.fenrir.yggdrasil.core.descriptor.ArtifactUpdateDescriptor;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.service.IApplicationUpdateService;
import org.fenrir.yggdrasil.core.service.IPreferenceService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140315
 */
public class ApplicationUpdateServiceImpl implements IApplicationUpdateService
{
    private final Logger log = LoggerFactory.getLogger(ApplicationUpdateServiceImpl.class);
    
    @Inject
    private IPreferenceService preferenceService;
    
    public void setPreferenceService(IPreferenceService preferenceService)
    {
        this.preferenceService = preferenceService;
    }

    @Override
    public void prepareUpdateFolder() throws ApplicationException
    {
        // S'esborra el contingut anterior del directori update
        File outputDir = new File(CoreConstants.UPDATE_FOLDER);            
        if(outputDir.exists()){
            try{
                FileUtils.cleanDirectory(outputDir);
            } 
            catch(IOException e){
                log.error("Error netejant el directori d'actualització: {}", e.getMessage(), e);
                throw new ApplicationException("Error preparant el directori d'actualització", e);
            }
        }        
        else{
            try{
                FileUtils.forceMkdir(outputDir);
            } 
            catch(IOException e){
                log.error("Error netejant el directori d'actualització: {}", e.getMessage(), e);
                throw new ApplicationException("Error preparant el directori d'actualització", e);
            }                                    
        }
    }
    
    @Override
    public Map<String, String> checkUpdateVersion() throws ApplicationException
    {
        Map<String, String> updateResult = new TreeMap<String, String>();
        
        XPath xpath = XPathFactory.newInstance().newXPath(); 
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            String applicationFilename = ApplicationContext.getInstance().getPluginRegistry().getArtifactsDefinition().getFile();
            // Per carregar l'stream desde el fitxer, cal que el path comenci per "/"
            if(!applicationFilename.startsWith("/")){
            	applicationFilename = "/" + applicationFilename;
            }
            Document applicationDocument = builder.parse(getClass().getResourceAsStream(applicationFilename));
            XPathExpression expression = xpath.compile(CoreConstants.UPDATE_APPLICATION_VERSION);
            String applicationVersion = (String)expression.evaluate(applicationDocument, XPathConstants.STRING);
            
            String urlUpdateSite = preferenceService.getProperty(CoreConstants.PREFERENCES_UPDATE_SITE);
            String channelUpdateSite = preferenceService.getProperty(CoreConstants.PREFERENCES_UPDATE_CHANNEL);
            // Es descarrega i es tracta el fitxer update.xml
            downloadFile(urlUpdateSite, CoreConstants.UPDATE_FILE);
            File updateFile = new File(CoreConstants.UPDATE_FILE_LOCATION);
            Document updateDocument = builder.parse(updateFile);            
            expression = xpath.compile(MessageFormat.format(CoreConstants.UPDATE_LAST_VERSION, channelUpdateSite));
            String updateLastVersion = (String)expression.evaluate(updateDocument, XPathConstants.STRING);
            // Cas que hi hagi una versió més nova es mira si es compatible
            expression = xpath.compile(MessageFormat.format(CoreConstants.UPDATE_MIN_VERSION, channelUpdateSite, updateLastVersion));
            String updateMinVersion = (String)expression.evaluate(updateDocument, XPathConstants.STRING);
            // Si la versió actual de l'aplicació és més petita que la mínima requerida s'haurà de descarregar l'aplicació sencera
            if(applicationVersion.compareTo(updateMinVersion)<0){
                log.info("La versió actual {} no pot ser actualitzada a la versió {}: Versió mínima requerida {}",
                        new Object[]{applicationVersion, updateLastVersion, updateMinVersion});                

                // Url de descarrega del distribuible
                // Es mira si es compatible
                expression = xpath.compile(MessageFormat.format(CoreConstants.UPDATE_DIST_URL, channelUpdateSite, updateLastVersion));
                String updateDistUrl = (String)expression.evaluate(updateDocument, XPathConstants.STRING);                    
                updateResult.put(applicationVersion, updateDistUrl);                    

                expression = xpath.compile(MessageFormat.format(CoreConstants.UPDATE_ARTIFACTS_URL, channelUpdateSite, applicationVersion));
                String updateArtifactsUrl = (String)expression.evaluate(updateDocument, XPathConstants.STRING);                    
                updateResult.put(updateLastVersion, updateArtifactsUrl);
            }
            else{
                expression = xpath.compile(MessageFormat.format(CoreConstants.UPDATE_ARTIFACTS_URL, channelUpdateSite, updateLastVersion));
                String updateArtifactsUrl = (String)expression.evaluate(updateDocument, XPathConstants.STRING);                    
                updateResult.put(updateLastVersion, updateArtifactsUrl);
            }
            
            return updateResult;
        }
        catch(Exception e){
            throw new ApplicationException("Error comprovant les actualitzacions", e);
        }
    }
    
    @Override
    public List<ArtifactUpdateDescriptor> checkArtifacts(String updateVersion, String urlArtifacts) throws ApplicationException
    {
        List<ArtifactUpdateDescriptor> artifactDescriptors = new ArrayList<ArtifactUpdateDescriptor>();
        
        XPath xpath = XPathFactory.newInstance().newXPath(); 
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            String applicationFilename = ApplicationContext.getInstance().getPluginRegistry().getArtifactsDefinition().getFile();
            // Per carregar l'stream desde el fitxer, cal que el path comenci per "/"
            if(!applicationFilename.startsWith("/")){
            	applicationFilename = "/" + applicationFilename;
            }
            Document applicationArtifactsDocument = builder.parse(getClass().getResourceAsStream(applicationFilename));            
            // En el cas que es pugui actualitzar, es mira quin fitxers cal descarregar
            downloadFile(urlArtifacts, CoreConstants.UPDATE_FILE_ARTIFACTS);
            File downloadedArtifactsFile = new File(CoreConstants.UPDATE_FILE_ARTIFACTS_LOCATION);
            Document downloadedArtifactsDocument = builder.parse(downloadedArtifactsFile);
            // Es recuperen les IDs dels artifacts instal.lats a l'aplicació
            List<String> applicationArtifacts = new ArrayList<String>();
            XPathExpression expression = xpath.compile(CoreConstants.UPDATE_ARTIFACT_IDS);
            NodeList idNodes = (NodeList)expression.evaluate(applicationArtifactsDocument, XPathConstants.NODESET);
            for(int i=0; i<idNodes.getLength(); i++){
                Node node = idNodes.item(i);
                String id = node.getNodeValue();
                applicationArtifacts.add(id);
            }
            // Es recuperen els IDs dels artifacts de l'actualització
            List<String> updateArtifacts = new ArrayList<String>();
            expression = xpath.compile(CoreConstants.UPDATE_ARTIFACT_IDS);
            idNodes = (NodeList)expression.evaluate(downloadedArtifactsDocument, XPathConstants.NODESET);
            for(int i=0; i<idNodes.getLength(); i++){
                Node node = idNodes.item(i);
                String id = node.getNodeValue();
                updateArtifacts.add(id);
            }
            // Es comproba si cal actualitzar            
            for(String id:updateArtifacts){
                // Si no existeix la ID dins als artifacts instalats vol dir que és nou
                if(applicationArtifacts.indexOf(id)<0){
                    if(log.isDebugEnabled()){
                        log.debug("Trobat artifact a instal.lar amb id {}", id);
                    }
                    expression = xpath.compile(MessageFormat.format(CoreConstants.UPDATE_ARTIFACT_PATH, id));
                    String updateArtifactPath = (String)expression.evaluate(downloadedArtifactsDocument, XPathConstants.STRING);
                    expression = xpath.compile(MessageFormat.format(CoreConstants.UPDATE_ARTIFACT_URL, id));
                    String updateArtifactUrl = (String)expression.evaluate(downloadedArtifactsDocument, XPathConstants.STRING);
                    
                    ArtifactUpdateDescriptor descriptor = new ArtifactUpdateDescriptor();
                    descriptor.setId(id);
                    descriptor.setType(ArtifactUpdateDescriptor.TYPE_CREATE_ARTIFACT);
                    descriptor.setDestinationPath(updateArtifactPath);
                    descriptor.setUrl(updateArtifactUrl);
                    artifactDescriptors.add(descriptor);
                }
                else{
                    expression = xpath.compile(MessageFormat.format(CoreConstants.UPDATE_ARTIFACT_VERSION, id));
                    String updateArtifactVersion = (String)expression.evaluate(downloadedArtifactsDocument, XPathConstants.STRING);
                    expression = xpath.compile(MessageFormat.format(CoreConstants.UPDATE_ARTIFACT_VERSION, id));
                    String applicationArtifactVersion = (String)expression.evaluate(applicationArtifactsDocument, XPathConstants.STRING);                    
                    // S'ha d'actualitzar tan en el cas que sigui una versió superior com una inferior per tractar els casos dels downgrades de llibreries
                    if(updateArtifactVersion.compareTo(applicationArtifactVersion)!=0){
                        if(log.isDebugEnabled()){
                            log.debug("Trobat artifact a actualitzar amb id {}", id);
                        }
                        expression = xpath.compile(MessageFormat.format(CoreConstants.UPDATE_ARTIFACT_PATH, id));
                        String applicationArtifactPath = (String)expression.evaluate(applicationArtifactsDocument, XPathConstants.STRING);
                        expression = xpath.compile(MessageFormat.format(CoreConstants.UPDATE_ARTIFACT_PATH, id));
                        String updateArtifactPath = (String)expression.evaluate(downloadedArtifactsDocument, XPathConstants.STRING);
                        expression = xpath.compile(MessageFormat.format(CoreConstants.UPDATE_ARTIFACT_URL, id));
                        String updateArtifactUrl = (String)expression.evaluate(downloadedArtifactsDocument, XPathConstants.STRING);
                        
                        ArtifactUpdateDescriptor descriptor = new ArtifactUpdateDescriptor();
                        descriptor.setId(id);
                        descriptor.setType(ArtifactUpdateDescriptor.TYPE_UPDATE_ARTIFACT);
                        descriptor.setSourcePath(applicationArtifactPath);
                        descriptor.setDestinationPath(updateArtifactPath);
                        descriptor.setUrl(updateArtifactUrl);
                        artifactDescriptors.add(descriptor);
                    }
                    applicationArtifacts.remove(id);
                }                                
            }
            // Els que han quedat a applicationArtifacts s'han d'eliminar
            for(String id:applicationArtifacts){
                if(log.isDebugEnabled()){
                    log.debug("Trobat artifact a eliminar amb id {}", id);
                }
                expression = xpath.compile(MessageFormat.format(CoreConstants.UPDATE_ARTIFACT_PATH, id));
                String applicationArtifactPath = (String)expression.evaluate(applicationArtifactsDocument, XPathConstants.STRING);
                
                ArtifactUpdateDescriptor descriptor = new ArtifactUpdateDescriptor();
                descriptor.setId(id);
                descriptor.setType(ArtifactUpdateDescriptor.TYPE_DELETE_ARTIFACT);
                descriptor.setSourcePath(applicationArtifactPath);
                artifactDescriptors.add(descriptor);
            }
            
            return artifactDescriptors;
        }
        catch(Exception e){
            throw new ApplicationException("Error comprovant les actualitzacions", e);
        }
    }
    
    @Override
    public void createActionsFile(List<ArtifactUpdateDescriptor> artifactDescriptors) throws ApplicationException
    {
        ArtifactUpdateActions actions = new ArtifactUpdateActions();
        actions.setActions(artifactDescriptors);
        saveActionsFile(actions);
    }

    @Override
    public ArtifactUpdateActions readActionsFile() throws ApplicationException
    {
        try{
            JAXBContext context = JAXBContext.newInstance(ArtifactUpdateActions.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            File inputFile = new File(CoreConstants.UPDATE_FILE_ACTIONS_LOCATION);
            if(!inputFile.exists()){
                throw new ApplicationException("No s'han trobat fitxers a descarregar");
            }
            return (ArtifactUpdateActions)unmarshaller.unmarshal(inputFile);             
        } 
        catch(JAXBException e){
            log.error("Error creant el fitxer descriptor d'actualitzacions a realitzar: {}", e.getMessage(), e);
            throw new ApplicationException("Error descarregant les actualitzacions", e);
        }
    }
    
    @Override
    public void saveActionsFile(ArtifactUpdateActions actions) throws ApplicationException
    {
        try{
            JAXBContext context = JAXBContext.newInstance(ArtifactUpdateActions.class);
            Marshaller marshaller = context.createMarshaller();
            File outputFile = new File(CoreConstants.UPDATE_FILE_ACTIONS_LOCATION);
            marshaller.marshal(actions, outputFile);
        } 
        catch(JAXBException e){
            log.error("Error creant el fitxer descriptor d'actualitzacions a realitzar: {}", e.getMessage(), e);
            throw new ApplicationException("Error comprovant les actualitzacions", e);
        }
    }
    
    @Override
    public void downloadFile(String url, String filename) throws ApplicationException
    {
        if(log.isDebugEnabled()){
            log.debug("Descarregant {} a {}", url, filename);
        }
        
        File outputDir = new File(CoreConstants.UPDATE_FOLDER);
        File outputFile = new File(outputDir, filename);                       
        try{
            // Es mira si cal crear els directoris intermedis
            String folder = FilenameUtils.getFullPathNoEndSeparator(outputFile.getAbsolutePath());
            FileUtils.forceMkdir(new File(folder));
            
            URI uri = new URI(url);
            HttpEntity entity = doGetRequest(uri);
            double length = entity.getContentLength();
            // Per si de cas...
            if(length<=0){
                length = 1;
            }
            InputStream inputStream = entity.getContent();            
            if(outputFile.exists()){
                outputFile.delete();
            }
            FileOutputStream outstream = new FileOutputStream(outputFile);
            try {
                byte[] buffer = new byte[CoreConstants.UPDATE_DOWNLOAD_BUFFER_SIZE];
                int size = -1;
                while ((size = inputStream.read(buffer)) != -1) {                    
                    outstream.write(buffer, 0, size);
                }
                outstream.flush();
            } 
            finally{
                outstream.close();
            }
        }
        catch(Exception e){
            throw new ApplicationException("Error descarregant fitxer " + url, e);
        }
    }
    
    private HttpEntity doGetRequest(URI uri) throws Exception
    {
        CookieStore cookieStore = new BasicCookieStore();
        HttpContext localContext = new BasicHttpContext();
        localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

        DefaultHttpClient httpclient = new DefaultHttpClient();
        /* Configuració del proxy */
        // S'indica que agafi el proxy per defecte indicat a la JRE
        ProxySelectorRoutePlanner routePlanner = new ProxySelectorRoutePlanner(
                httpclient.getConnectionManager().getSchemeRegistry(),
                ProxySelector.getDefault());  
        httpclient.setRoutePlanner(routePlanner);
        HttpGet httpget = new HttpGet(uri);
        httpget.setHeader("User-Agent", CoreConstants.UPDATE_DEFAULT_USER_AGENT);

        HttpResponse response = httpclient.execute(httpget, localContext);
        HttpEntity entity = response.getEntity();
        if(entity!=null && response.getStatusLine().getStatusCode()==200){
            return entity;
        } 
        else{
            throw new ApplicationException("No s'ha pogut contactar amb el servidor " + uri);
        }
    }
}
