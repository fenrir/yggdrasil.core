package org.fenrir.yggdrasil.core.service.impl;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import javax.inject.Inject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Document;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.CoreConstants;
import org.fenrir.yggdrasil.core.definition.PreferencesDefinition;
import org.fenrir.yggdrasil.core.event.IWorkspaceEventListener;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.exception.PreferenceException;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.yggdrasil.core.service.IWorkspaceAdministrationService;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20150109
 */
public class WorkspaceAdministrationServiceImpl implements IWorkspaceAdministrationService 
{
	private static final String PREFERENCE_WORKSPACE_FILEPATH_PATTERN = "{0}/preferences/{1}.xml";
    private static final String DEFAULT_WORKSPACES_PREFERENCES_FILE = "org/fenrir/yggdrasil/core/conf/workspaces.xml";
    private static final String WORKSPACES_PREFERENCES_FILE = "workspaces.xml";
    private static final String DEFAULT_FRAMEWORK_PREFERENCES_FILE = "org/fenrir/yggdrasil/core/conf/org.fenrir.yggdrasil.core.default.xml";
    private static final String UPDATE_FRAMEWORK_PREFERENCES_FILE = "org/fenrir/yggdrasil/core/conf/org.fenrir.yggdrasil.core.update.xml";
	
    private final Logger log = LoggerFactory.getLogger(WorkspaceAdministrationServiceImpl.class);
	
    @Inject
    private IPreferenceService preferenceService;
    
    @Inject
    private IEventNotificationService eventNotificationService;
	    	
    public void setPreferenceService(IPreferenceService preferenceService)
    {
        this.preferenceService = preferenceService;
    }
    
    public void setEventNotificationService(IEventNotificationService eventNotificationService)
    {
        this.eventNotificationService = eventNotificationService;
    }	
	
    @Override
    public List<String> getWorkspacesList() throws ApplicationException
    {
        List<String> workspaces = new ArrayList<String>();
        
        File workspacesFile = new File(WORKSPACES_PREFERENCES_FILE);
        if(workspacesFile.exists()){           
            try{
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                XPath xpath = XPathFactory.newInstance().newXPath();
                Document workspacesDocument = builder.parse(workspacesFile);        

                XPathExpression folderExpr = xpath.compile("//workspaces/workspace/folder");
                NodeList nodes = (NodeList)folderExpr.evaluate(workspacesDocument, XPathConstants.NODESET);
                for(int i=0; i<nodes.getLength(); i++){
                    Node node = nodes.item(i);
                    String folder = node.getTextContent();
                    // Es reemplaça els separadors de més que es posen en les rutes en Windows
                    workspaces.add(folder.replace("\\\\", "\\"));
                }
            }
            catch(Exception e){
                throw new ApplicationException(e.getMessage(), e);
            }
        }
        
        return workspaces;
    }
    
    @Override
    public boolean loadDefaultWorkspace() throws PreferenceException, ApplicationException
    {
        URL workspacesURL;
        File workspacesFile = new File(WORKSPACES_PREFERENCES_FILE);
        if(!workspacesFile.exists()){
            workspacesURL = getClass().getClassLoader().getResource(DEFAULT_WORKSPACES_PREFERENCES_FILE);
        }
        else{
            try{
                workspacesURL = workspacesFile.toURI().toURL();
            }
            catch(MalformedURLException e){
                log.error("Error al carregar fitxer de workspaces. Carregant per defecte: " + e.getMessage(), e);
                workspacesURL = getClass().getResource(DEFAULT_WORKSPACES_PREFERENCES_FILE);
            }
        }

        preferenceService.loadPreferencesFile(IPreferenceService.WORKSPACES_PREFERENCES_ID, workspacesURL);
        preferenceService.createPreferences(IPreferenceService.TRANSIENT_PREFERENCES_ID);
        String defaultWorkspace = preferenceService.getProperty(CoreConstants.PREFERENCES_WORKSPACE_RESTART_FOLDER);
        // Si no hi ha workspace indicat pel reinici es busca el per defecte
        if(StringUtils.isEmpty(defaultWorkspace)){
            defaultWorkspace = preferenceService.getProperty(CoreConstants.PREFERENCES_WORKSPACE_DEFAULT_FOLDER);
        }
        else{
            clearRestartWorkspace();
        }

        URL defaultConfigurationUrl = getClass().getClassLoader().getResource(DEFAULT_FRAMEWORK_PREFERENCES_FILE);
        preferenceService.loadPreferencesFile(IPreferenceService.FRAMEWORK_PREFERENCES_ID, defaultConfigurationUrl);
        // Per cada una de les definicions de preferències, es carreguen els valors per defecte
        for(PreferencesDefinition definition:ApplicationContext.getInstance().getPluginRegistry().getPreferencesDefinitions()){
        	String defaultConfFilename = definition.getDefaultFile().getFile();
        	defaultConfigurationUrl = getClass().getClassLoader().getResource(defaultConfFilename);
            preferenceService.loadPreferencesFile(definition.getId(), defaultConfigurationUrl);
        }
        
        if(StringUtils.isNotBlank(defaultWorkspace)){
            loadWorkspace(defaultWorkspace);

            if(log.isDebugEnabled()){
                log.debug("Carregada configuració del workspace {}", defaultWorkspace);
            }

            return true;
        }
        else{
            if(log.isDebugEnabled()){
                log.debug("Carregada configuració per defecte");
            }

            return false;
        }
    }

    @Override
    public void loadWorkspace(String workspaceFolder) throws PreferenceException, ApplicationException
    {   
        try{
            eventNotificationService.notifyNamedEvent(IWorkspaceEventListener.class, IWorkspaceEventListener.EVENT_BEFORE_WORKSPACE_LOAD_ID);
        }
        catch(Exception e){
            log.error("Error notificant event pre-càrrega del workspace: {}", e.getMessage(), e);
            throw new ApplicationException(e);
        }
        
        if(preferenceService.getProperty("//transient/workspace/folder")!=null){
            preferenceService.setProperty("//transient/workspace/folder", workspaceFolder);
        }
        else{
            preferenceService.addProperty("//transient workspace/folder", workspaceFolder);
        }
        
        // Es carreguen els valors del workspace per les preferencies del framework
        loadPreferencesFromWorkspace(IPreferenceService.FRAMEWORK_PREFERENCES_ID, DEFAULT_FRAMEWORK_PREFERENCES_FILE, UPDATE_FRAMEWORK_PREFERENCES_FILE);
        // Per cada una de les definicions de preferències, es carreguen els valors del workspaces
        for(PreferencesDefinition definition:ApplicationContext.getInstance().getPluginRegistry().getPreferencesDefinitions()){
        	loadPreferencesFromWorkspace(definition.getId(), definition.getDefaultFile().getFile(), definition.getUpdateFile().getFile());
        }

        // Es carrega el contexte relatiu al workspace
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        applicationContext.configureContext(CoreConstants.EXECUTION_CONTEXT_WORKSPACE_INITIALIZED);

        try{
            eventNotificationService.notifyNamedEvent(IWorkspaceEventListener.class, IWorkspaceEventListener.EVENT_WORKSPACE_LOADED_ID);
        }
        catch(Exception e){
            log.error("Error notificant event de càrrega del workspace: {}", e.getMessage(), e);
            throw new ApplicationException(e);
        }
    }

    @Override
    public void unloadWorkspace() throws ApplicationException
    {
        try{
            eventNotificationService.notifyNamedEvent(IWorkspaceEventListener.class, IWorkspaceEventListener.EVENT_BEFORE_WORKSPACE_UNLOAD_ID);
        }
        catch(Exception e){
            log.error("Error notificant event pre-descàrrega del workspace: {}", e.getMessage(), e);
            throw new ApplicationException(e);
        }
        
        preferenceService.removePreferences(IPreferenceService.FRAMEWORK_PREFERENCES_ID);
        preferenceService.clearProperty("//transient/workspace/folder");
        // Per cada una de les definicions de preferències, s'eliminen de la memòria
        for(PreferencesDefinition definition:ApplicationContext.getInstance().getPluginRegistry().getPreferencesDefinitions()){
        	preferenceService.removePreferences(definition.getId());
        }

        ApplicationContext.getInstance().disposeContext(CoreConstants.EXECUTION_CONTEXT_WORKSPACE_INITIALIZED);

        try{
            eventNotificationService.notifyNamedEvent(IWorkspaceEventListener.class, IWorkspaceEventListener.EVENT_WORKSPACE_UNLOADED_ID);
        }
        catch(Exception e){
            log.error("Error notificant event de descàrrega del workspace: {}", e.getMessage(), e);
            throw new ApplicationException(e);
        }
    }

    @Override
    public void addWorkspace(String workspaceFolder, boolean isDefault) throws PreferenceException
    {
        if(preferenceService.getProperty("//workspaces/workspace[folder='"+workspaceFolder+"']/folder")==null){
            preferenceService.addProperty("//workspaces workspace/folder", workspaceFolder);
        }

        if(isDefault){    		
            // Si hi ha algun altre workspace per defecte, se li treu el flag
            String oldDefaultWorkspaceFolder = preferenceService.getProperty(CoreConstants.PREFERENCES_WORKSPACE_DEFAULT_FOLDER);
            preferenceService.clearProperty("//workspaces/workspace[folder='"+oldDefaultWorkspaceFolder+"']/@default");

            // S'especifica el nou directori com el nou workspace per defecte 
            preferenceService.addProperty("//workspaces/workspace[folder='"+workspaceFolder+"'] @default", "true");
        }
        else{
            preferenceService.clearProperty("//workspaces/workspace[folder='"+workspaceFolder+"']/@default");
        }

        // Es guarda el fitxer de workspaces
        File workspacesFile = new File(WORKSPACES_PREFERENCES_FILE);
        if(!workspacesFile.exists()){
            preferenceService.save(IPreferenceService.WORKSPACES_PREFERENCES_ID, WORKSPACES_PREFERENCES_FILE);
        }
        else{
            preferenceService.save(IPreferenceService.WORKSPACES_PREFERENCES_ID);
        } 
    }
    
    @Override
    public boolean isWorkspaceLoaded()
    {
    	return preferenceService.getProperty(CoreConstants.PREFERENCES_WORKSPACE_CURRENT)!=null;
    }    
    
    @Override
    public String getCurrentWorkspaceFolder()
    {
        return preferenceService.getProperty(CoreConstants.PREFERENCES_WORKSPACE_CURRENT);
    }

    @Override
    public void setRestartWorkspace(String folder) throws PreferenceException
    {
        preferenceService.addProperty("//workspaces/workspace[folder='"+folder+"'] @restart", Boolean.TRUE.toString());
        preferenceService.save(IPreferenceService.WORKSPACES_PREFERENCES_ID);
    }

    @Override
    public void clearRestartWorkspace() throws PreferenceException
    {
        String restartFolder = preferenceService.getProperty(CoreConstants.PREFERENCES_WORKSPACE_RESTART_FOLDER);
        if(StringUtils.isNotBlank(restartFolder)){
            preferenceService.clearProperty("//workspaces/workspace[folder='"+restartFolder+"']/@restart");
            preferenceService.save(IPreferenceService.WORKSPACES_PREFERENCES_ID);
        }
    }
    
    private void loadPreferencesFromWorkspace(String id, String defaultPreferenceFile, String updatePreferenceFile) throws PreferenceException, ApplicationException
    {
    	String preferencesFilePath = MessageFormat.format(PREFERENCE_WORKSPACE_FILEPATH_PATTERN, new Object[]{getCurrentWorkspaceFolder(), id});
        File preferencesFile = new File(preferencesFilePath);   	
        // Si no existeix el fitxer de configuració, s'inicialitza el valor de les seves preferencies per defecte
        if(!preferencesFile.exists()){
            URL defaultPreferencesUrl = getClass().getClassLoader().getResource(defaultPreferenceFile);
            preferenceService.loadPreferencesFile(id, defaultPreferencesUrl);
            preferenceService.save(id, preferencesFile.getAbsolutePath());
        }        
        else{
            try{                
                // Si es tracta d'una versió anterior, s'actualitza
                if(updateWorkspacePreferences(id, preferencesFile, updatePreferenceFile)){
                    preferenceService.save(id, preferencesFile.getAbsolutePath());
                }
                // En cas contrari es carreguen directament les preferències del workspace
                else{
                    preferenceService.loadPreferencesFile(id, preferencesFile);
                }
            }
            catch(Exception e){
                log.error("Error en actualitzar les preferències del workspace: {}", e.getMessage(), e);
                throw new ApplicationException(e);
            }
        }
    }
    
    private boolean updateWorkspacePreferences(final String id, final File configurationFile, final String updateFile) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        XPath xpath = XPathFactory.newInstance().newXPath();
        Document configurationDocument = builder.parse(configurationFile);        

        @SuppressWarnings("serial")
		String strVersionExpr = StrSubstitutor.replace(CoreConstants.PREFERENCES_VERSION, new HashMap<String, String>(){
        	{
        		put("id", id);
        	}
        });
        
        XPathExpression versionExpr = xpath.compile(strVersionExpr);
        int currentVersion = ((Double)versionExpr.evaluate(configurationDocument, XPathConstants.NUMBER)).intValue();
        int applicationVersion = Integer.parseInt(preferenceService.getProperty(strVersionExpr, "0"));
        if(applicationVersion>currentVersion){
        	Document updateDocument = builder.parse(getClass().getClassLoader().getResourceAsStream(updateFile));

            XPathExpression preferencesExpr = xpath.compile("//preference");
            NodeList updatePreferenceNodes = (NodeList)preferencesExpr.evaluate(updateDocument, XPathConstants.NODESET);
            for(int i=0; i<updatePreferenceNodes.getLength(); i++){
                Node node = updatePreferenceNodes.item(i);
                String preferenceId = node.getAttributes().getNamedItem("id").getNodeValue();
                XPathExpression selectorExpr = xpath.compile("//preference[@id='" + preferenceId + "']/selector");
                String selector = (String)selectorExpr.evaluate(updateDocument, XPathConstants.STRING);
                XPathExpression updaterExpr = xpath.compile("//preference[@id='" + preferenceId + "']/update[@version<='" + currentVersion + "'][last()]");
                String updater = (String)updaterExpr.evaluate(updateDocument, XPathConstants.STRING);
                
                if(!StringUtils.isEmpty(updater)){
                    XPathExpression valueExpr = xpath.compile(updater);
                    String value = (String)valueExpr.evaluate(configurationDocument, XPathConstants.STRING);
                    if(StringUtils.isNotBlank(value)){
                        preferenceService.setProperty(selector, value);
                    }
                }
                else{
                    log.warn("La propietat {} no té updater vàlid", preferenceId);
                }
            }
            
            return true;
        }

        return false;
    }
}