package org.fenrir.yggdrasil.core.service;

import java.util.Collection;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20131203
 */
@SuppressWarnings("rawtypes")
public interface IEventNotificationService 
{		
    public void notifyEvent(Class listenerInterface) throws Exception;
    public void notifyEvent(Class listenerInterface, Object... parameters) throws Exception;
    public void notifyNamedEvent(Class listenerInterface, String eventName) throws Exception;
    public void notifyNamedEvent(Class listenerInterface, String eventName, Object... parameters) throws Exception;
    public void addListener(Class listenerInterface, Object instance);
    public void addListeners(Class listenerInterface, Collection instances);
    public void removeListener(Object instance);
    public void removeListeners(Class listenerInterface);
    public void removeAllListeners();
}