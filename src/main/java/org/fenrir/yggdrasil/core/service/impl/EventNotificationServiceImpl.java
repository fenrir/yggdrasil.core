package org.fenrir.yggdrasil.core.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.event.annotation.EventMethod;

/**
 * TODO 1.0 javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140724
 */
public class EventNotificationServiceImpl implements IEventNotificationService
{
    private final Logger log = LoggerFactory.getLogger(EventNotificationServiceImpl.class);
    
    @SuppressWarnings("rawtypes")
    private Map<Class, RegistryEntry> registry = new HashMap<Class, RegistryEntry>();
	
    @Override
    @SuppressWarnings("rawtypes")
    public void notifyEvent(Class listenerInterface) throws Exception
    {
        notifyEvent(listenerInterface, new Object[]{});
    }
    
    @Override
    @SuppressWarnings("rawtypes")
    public void notifyEvent(Class listenerInterface, Object... parameters) throws Exception
    {
        RegistryEntry entry = registry.get(listenerInterface);
        if(entry!=null){
        	// 
            List<Object> registeredInstances = entry.getRegisteredInstances();
            Method eventMethod = entry.getEventMethod();
            try{								
                for(Object listener:registeredInstances){
                    Method instanceMethod = listener.getClass().getMethod(eventMethod.getName(), eventMethod.getParameterTypes());
                    instanceMethod.invoke(listener, parameters);
                }							
            }
            catch(InvocationTargetException e){
            	log.error("Error notificant event: {}", e.getCause().getMessage(), e);
            	throw new Exception(e.getCause().getMessage(), e.getCause());
            }
            catch(Exception e){
                log.error("Error notificant event: {}", e.getMessage(), e);
                throw e;
            }
        }
    }
	
    @Override
    @SuppressWarnings("rawtypes")
    public void notifyNamedEvent(Class listenerInterface, String eventName) throws Exception
    {
        notifyNamedEvent(listenerInterface, eventName, new Object[]{});
    }
    
    @Override
    @SuppressWarnings("rawtypes")
    public void notifyNamedEvent(Class listenerInterface, String eventName, Object... parameters) throws Exception
    {
        RegistryEntry entry = registry.get(listenerInterface);
        if(entry!=null){
            List<Object> registeredInstances = entry.getRegisteredInstances();
            Method eventMethod = entry.getEventMethod(eventName);
            try{
                for(Object listener:registeredInstances){				
                    Method instanceMethod = listener.getClass().getMethod(eventMethod.getName(), eventMethod.getParameterTypes());
                    instanceMethod.invoke(listener, parameters);
                }							
            }
            catch(InvocationTargetException e){
            	log.error("Error notificant event: {}", e.getCause().getMessage(), e);
            	throw new Exception(e.getCause().getMessage(), e.getCause());
            }
            catch(Exception e){
                log.error("Error notificant event: {}", e.getMessage(), e);
                throw e;
            }
        }
    }	

    @Override
    @SuppressWarnings("rawtypes")
    public void addListener(Class listenerInterface, Object instance) 
    {	
        RegistryEntry entry = getEntry(listenerInterface);		
        if(entry!=null){
            entry.addRegisteredInstance(instance);
        }		
    }

    @Override
    @SuppressWarnings("rawtypes")
    public void addListeners(Class listenerInterface, Collection instances)
    {
        RegistryEntry entry = getEntry(listenerInterface);
        if(entry!=null){
            for(Object instance:instances){
                entry.addRegisteredInstance(instance);
            }
        }
    }
	
    @SuppressWarnings("rawtypes")
    private RegistryEntry getEntry(Class listenerInterface)
    {
        RegistryEntry entry = registry.get(listenerInterface);
        if(entry==null){
            entry = new RegistryEntry();
            Method[] interfaceMethods = listenerInterface.getDeclaredMethods();
            for(Method method:interfaceMethods){
                EventMethod annotation = method.getAnnotation(EventMethod.class); 
                if(annotation!=null){
                    String eventName = annotation.eventName();
                    entry.putEventMethod(eventName, method);
                }
                else if(entry.getEventMethod()==null){
                    entry.putEventMethod(method);
                }
                else{
                    // TODO llançar excepció perque ja s'ha registrat l'event per defecte
                }
            }			
            registry.put(listenerInterface, entry);
        }

        return entry;
    }
	
    @Override
    @SuppressWarnings("rawtypes")
    public void removeListener(Object instance)
    {
        Iterator<Class> iterator = registry.keySet().iterator();
        boolean found = false;
        while(iterator.hasNext() && !found){
            Class clazz =  iterator.next();
            if(clazz.isInstance(instance)){
                registry.get(clazz).removeRegisteredInstance(instance);				
            }
        }			
    }
	
    @Override
    @SuppressWarnings("rawtypes")
    public void removeListeners(Class listenerInterface) 
    {	
        RegistryEntry entry = registry.get(listenerInterface);
        entry.removeAllRegisteredInstances();		
    }

    @Override
    public void removeAllListeners() 
    {	
        registry.clear();
    }
    
    private class RegistryEntry
    {		
        private static final String DEFAULT_EVENT_NAME = "default";

        private Map<String, Method> eventMethods;	
        private List<Object> registeredInstances;

        public RegistryEntry()
        {			
            this.eventMethods = new HashMap<String, Method>();	
            /* Es fa servir una instancia de CopyOnWriteArrayList per evitar errors de concurrecia sobre la llista
             * Quan es crea un iterador sobre la llista, aquest només te en compte els elements en el moment de la seva creació.
             * És més costós en termes de consum de memòria que l'ArrayList, però és més segur.
             */
            this.registeredInstances = new CopyOnWriteArrayList<Object>();
        }				

        public Method getEventMethod()
        {
            return getEventMethod(DEFAULT_EVENT_NAME);
        }

        public Method getEventMethod(String eventName)
        {
            return eventMethods.get(eventName);
        }

        public void putEventMethod(Method method)
        {
            putEventMethod(DEFAULT_EVENT_NAME, method);
        }

        public void putEventMethod(String eventName, Method method)
        {
            eventMethods.put(eventName, method);
        }				

        public List<Object> getRegisteredInstances()
        {
            return registeredInstances;
        }

        public void addRegisteredInstance(Object instance)
        {
            registeredInstances.add(instance);
        }

        public void removeRegisteredInstance(Object instance)
        {
            registeredInstances.remove(instance);
        }

        public void removeAllRegisteredInstances()
        {
            registeredInstances.clear();
        }
    }
}