package org.fenrir.yggdrasil.core.service.impl;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import org.apache.commons.configuration.CombinedConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.tree.UnionCombiner;
import org.apache.commons.configuration.tree.xpath.XPathExpressionEngine;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.exception.PreferenceException;
import org.fenrir.yggdrasil.core.service.IPreferenceService;

/**
 * TODO v1.0 Javadoc
 * TODO v0.2 Quan es passi l'aplicació a plugins, partir les preferencies segons a qui pertanyin
 * @author Antonio Archilla Nava
 * @version v0.1.20121215
 */
public class PreferenceServiceImpl implements IPreferenceService
{    
    // Instancia del log
    private Logger log = LoggerFactory.getLogger(PreferenceServiceImpl.class);
    
    private CombinedConfiguration configuration; 

    public PreferenceServiceImpl()
    {
    	configuration = new CombinedConfiguration(new UnionCombiner());
    	configuration.setExpressionEngine(new XPathExpressionEngine());
    }
    
    @Override
    public void loadPreferencesFile(String id, File file) throws PreferenceException
    {
    	try{
            URL url = file.toURI().toURL();
            loadPreferencesFile(id, url);
        }
        catch(MalformedURLException e){
            log.error("Error al carregar preferencies desde el fitxer {}: {}", new Object[]{file.getAbsolutePath(), e.getMessage(), e});
            throw new PreferenceException("Error al carregar preferencies desde el fitxer " + file.getAbsolutePath() + ": " + e.getMessage(), e );
        }
    }
    
    @Override
    public void loadPreferencesFile(String id, URL url) throws PreferenceException
    {
    	try{
            XMLConfiguration partialConfiguration = new XMLConfiguration(url);    		
            // Es substitueix la configuració existent amb la id especificada
            configuration.removeConfiguration(id);
            configuration.addConfiguration(partialConfiguration, id);
    	}
    	catch(ConfigurationException e){
            log.error("Error al carregar preferencies desde la url {}: {}", new Object[]{url.toString(), e.getMessage(), e});
            throw new PreferenceException("Error al carregar preferencies desde la url " + url.toString() + ": " + e.getMessage(), e);
    	}
    }
    
    @Override
    public void createPreferences(String id)
    {
    	XMLConfiguration partialConfiguration = new XMLConfiguration();
    	HierarchicalConfiguration.Node rootNode = new HierarchicalConfiguration.Node(configuration.getRootNode().getName());
        rootNode.addChild(new HierarchicalConfiguration.Node(id));
        partialConfiguration.setRoot(rootNode);
        configuration.addConfiguration(partialConfiguration, id);
    }
    
    @Override
    public void removePreferences(String id)
    {
        configuration.removeConfiguration(id);
    }
    	
    @Override
    public List<String> getProperties(String key)
    {
        return configuration.getList(key);        
    }
    
    @Override
    public String getProperty(String key)
    {
        return configuration.getString(key);
    }        
    
    @Override
    public String getProperty(String key, String defaultValue)
    {
    	return StringUtils.defaultString(getProperty(key), defaultValue);
    }
    
    @Override
    public String getProperty(String key, Map<String, String> variables)
    {
    	StrSubstitutor substitutor = new StrSubstitutor(variables);
    	key = substitutor.replace(key);
    	return getProperty(key);
    }
    
    @Override
    public String getProperty(String key, Map<String, String> variables, String defaultValue)
    {
    	String value = getProperty(key, variables);
    	return StringUtils.defaultString(value, defaultValue);
    }
	
    @Override
    public void setProperty(String key, String value)
    {
    	configuration.setProperty(key, value);
    }
    
    @Override
    public void addProperty(String key, String value)
    {
    	configuration.addProperty(key, value);
    }
    
    @Override
    public void clearProperty(String key)
    {
    	configuration.clearProperty(key);
    }
	
    @Override
    public void save(String id) throws PreferenceException
    {
    	try{				    	    	    
            XMLConfiguration preferences = (XMLConfiguration)configuration.getConfiguration(id);	    	
            preferences.save();	    	
        }
        catch(ConfigurationException e){
            log.error("Error al guardar preferències al workspace actual: {}", e.getMessage(), e);
            throw new PreferenceException("Error al guardar preferències al workspace actual: " + e.getMessage(), e);
        }
    }
    
    @Override
    public void save(String id, String filePath) throws PreferenceException
    {
        try{				    	    	    	
            XMLConfiguration preferences = (XMLConfiguration)configuration.getConfiguration(id);	    	    		    			    	
            preferences.save(new File(filePath));
	    	
            // Es recarreguen les preferencies guardades amb les dades guardades
            XMLConfiguration newPreferences = new XMLConfiguration(filePath);
            configuration.removeConfiguration(id);    
            configuration.addConfiguration(newPreferences, id);
        }
        catch(ConfigurationException e){
            log.error("Error al guardar preferències al workspace actual: {}", e.getMessage(), e);
            throw new PreferenceException("Error al guardar preferències al workspace actual: " + e.getMessage(), e);
        }
    }        
}