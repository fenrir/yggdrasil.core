package org.fenrir.yggdrasil.core.service;

import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20131202
 */
public interface ITaskLauncherService 
{
    public void initializeTasks() throws ApplicationException;
    public void scheduleTask(String id);
    public void scheduleTask(String id, int initialDelay);
    public void shutdownTask(String id);
    public void shutdownAllTasks();
    public boolean isTaskScheduled(String id);
}
