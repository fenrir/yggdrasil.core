package org.fenrir.yggdrasil.core.service;

import java.util.List;
import java.util.Map;
import org.fenrir.yggdrasil.core.descriptor.ArtifactUpdateActions;
import org.fenrir.yggdrasil.core.descriptor.ArtifactUpdateDescriptor;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20131215
 */
public interface IApplicationUpdateService 
{
    public void prepareUpdateFolder() throws ApplicationException;
    public Map<String, String> checkUpdateVersion() throws ApplicationException;
    public List<ArtifactUpdateDescriptor> checkArtifacts(String updateVersion, String urlArtifacts) throws ApplicationException;
    public void createActionsFile(List<ArtifactUpdateDescriptor> artifactDescriptors) throws ApplicationException;
    public ArtifactUpdateActions readActionsFile() throws ApplicationException;
    public void saveActionsFile(ArtifactUpdateActions actions) throws ApplicationException;
    public void downloadFile(String url, String filename) throws ApplicationException;
}
