package org.fenrir.yggdrasil.core.service;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Map;
import org.fenrir.yggdrasil.core.exception.PreferenceException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20141011
 */
public interface IPreferenceService 
{
    public static final String CONFIGURATION_VALUE_TRUE = "true";
    public static final String CONFIGURATION_VALUE_FALSE = "false";
	
    public static final String WORKSPACES_PREFERENCES_ID = "workspaces";
    public static final String FRAMEWORK_PREFERENCES_ID = "org.fenrir.yggdrasil.core";
    public static final String TRANSIENT_PREFERENCES_ID = "transient";
    
    public void loadPreferencesFile(String id, File file) throws PreferenceException;
    public void loadPreferencesFile(String id, URL url) throws PreferenceException;
    public void createPreferences(String id);
    public void removePreferences(String id);
    public List<String> getProperties(String key);
    public String getProperty(String key);	
    public String getProperty(String key, String defaultValue);
    public String getProperty(String key, Map<String, String> variables);
    public String getProperty(String key, Map<String, String> variables, String defaultValue);
    public void setProperty(String key, String value);
    public void addProperty(String key, String value);
    public void clearProperty(String key);
    public void save(String id) throws PreferenceException;
    public void save(String id, String filePath) throws PreferenceException;
}