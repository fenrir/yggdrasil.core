package org.fenrir.yggdrasil.core.service;

import java.util.List;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.exception.PreferenceException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20131202
 */
public interface IWorkspaceAdministrationService 
{
    public List<String> getWorkspacesList() throws ApplicationException;
    public boolean loadDefaultWorkspace() throws PreferenceException, ApplicationException;
    public void loadWorkspace(String workspaceFolder) throws PreferenceException, ApplicationException;
    /**
     * TODO Verificar la necessitat del mètode
     * @throws ApplicationException
     */
    public void unloadWorkspace() throws ApplicationException;
    public void addWorkspace(String workspaceFolder, boolean isDefault) throws PreferenceException;
    public boolean isWorkspaceLoaded();
    public String getCurrentWorkspaceFolder();
    public void setRestartWorkspace(String folder) throws PreferenceException;
    public void clearRestartWorkspace() throws PreferenceException;
}